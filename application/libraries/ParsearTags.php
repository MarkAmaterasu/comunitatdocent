<?php

// Esta clase se encarga de parsear lo interesado en la creación de temas y respuestas del foro.
class ParsearTags {

    public static function parsear($mensaje) {
        // Citar
        $mensaje = str_replace("[citar]", "</strong><blockquote><hr size='2' /><strong>", $mensaje);
        $mensaje = str_replace("[/citar]", "</strong><hr size='2' /><strong>", $mensaje);
        // Negrita
        $mensaje = str_replace("[negreta]", "<b>", $mensaje);
        $mensaje = str_replace("[/negreta]", "</b>", $mensaje);
         // Listas
        $mensaje = str_replace("[llista]", "<ol style='
    width: 150px;
    padding-left: 20px;
    margin-left: 40%;
'>", $mensaje);
        $mensaje = str_replace("[/llista]", "</ol>", $mensaje);
        $mensaje = str_replace("[punt]", "<li type='disc'>", $mensaje);
        // Cursiva
        $mensaje = str_replace("[cursiva]", "<i>", $mensaje);
        $mensaje = str_replace("[/cursiva]", "</i>", $mensaje);
        //Subrayadado
        $mensaje = str_replace("[sub]", "<u>", $mensaje);
        $mensaje = str_replace("[/sub]", "</u>", $mensaje);
        // Imagen
        $mensaje = str_replace("[img", "<img height='100' width='100' class='materialboxed responsive-img valign centrado'", $mensaje);
        $mensaje = str_replace("urlimg", "src", $mensaje);
        // Enlace
        $mensaje = str_replace("[enlace", "<a target='_blank'", $mensaje);
        $mensaje = str_replace("[/enlace]", "</a>", $mensaje);   
        $mensaje = str_replace("url", "href", $mensaje);    
        
        $mensaje = str_replace("]", ">", $mensaje);       
        
        // Devuelve el mensaje con html.
        return $mensaje;
    }

}

?>