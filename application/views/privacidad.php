<div class="container contenido">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel z-depth-3">              
                <span class="black-text flow-text">
                    <h2 class="center-align">Política de privacitat i de participació</h2>                    
                    <p>Quan vostè ens facilita informació de caràcter personal a través del lloc web (d'ara endavant, el “Lloc web”), Comunitat Docent respecta la seva intimitat i els drets que li reconeix la normativa sobre protecció de dades de caràcter personal. Per això, és important que entengui que informació recaptem sobre vostè durant la seva visita i què fem amb aquesta informació. La seva visita al Lloc web està subjecta a la present Política sobre Tractament de Dades Personals:</p>
                    <p>No comunicarem a tercers la seva informació de caràcter personal, excepte en la forma establerta en aquesta Política sobre Privadesa i
Tractament de Dades o en els avisos establerts per a cada supòsit en què es recullin les seves dades personals.</p>
                    <p>
Comunitat Docent podrà revelar qualsevol informació, incloent dades de caràcter personal, que consideri necessària per donar compliment a les obligacions legals.</p>
                    <p>
COMUNITAT DOCENT NO RECAPTARÀ INFORMACIÓ PERSONAL SOBRE VOSTÈ Tret que TINGUI COM A MÍNIM CATORZE (14) ANYS. SI FOS MENOR D'AQUESTA EDAT, HAURÀ D'ABSTENIR-SE DE FACILITAR-NOS INFORMACIÓ DE CARÀCTER PERSONAL.</p>
                    <p><u>Recollida i Ús de la Informació</u></p>
                    <p>En facilitar-nos les seves dades de caràcter personal en el Lloc web, està expressant la seva acceptació al tractament i comunicació de les seves dades personals en la forma contemplada en aquesta Política sobre Tractament de Dades Personals. Si prefereix que Comunitat Docent no recapti informació personal sobre vostè, preguem que no ens la faciliti.</p>
                    <p>
Comunitat Docent podrà utilitzar la informació de caràcter personal que ens faciliti de forma dissociada (sense identificació personal) per a finalitats internes, com pot ser l'elaboració d'estadístiques. Així, Comunitat Docent podrà recaptar, emmagatzemar o acumular determinada informació de caràcter no personal referent al seu ús del Lloc web, com per exemple aquella informació que indiqui quins de les nostres pàgines són més populars.</p>
                    <p>
Les dades proporcionades durant el procés de registre s'utilitzaran per poder participar en la web.</p>
                    <p>Les dades ofertes en utilitzar el formulari de contacte s'utilitzaran únicament per respondre a aquest contacte si escau.</p>
                    <p><u>Enllaços</u></p>   
                <p>El Lloc web podrà contenir enllaços a o des d'altres llocs web. Ha de saber que Comunitat Docent no es responsabilitza de les pràctiques que, en matèria de tractament de dades personals, segueixen altres llocs web. La present Política sobre Tractament de Dades Personals és aplicable solament a la informació que recaptem en el Lloc web. Li aconsellem llegir les polítiques sobre tractament de dades personals d'altres llocs web amb els quals enllaci a o des del nostre Lloc web o que visiti d'una altra forma.</p>
                
                <p><u>Comentaris públics</u></p>

                <p>Sempre que un usuari escrigui un comentari públicament, aquest estarà disponible per al públic en general. La web no es fa responsable de la informació confidencial pròpia publicada per un mateix usuari en el Lloc web, sent l'usuari l'únic responsable de què publica.</p>
                
                <p><u>Acceptació dels Termes del Servei i Normes de la Comunitat</u></p>
                <p>SI US PLAU, LLEGEIXI AMB DETENIMENT LES CONDICIONS D'ÚS ABANS D'UTILITZAR EL PRESENT LLOC. PODEN SEMBLAR-LI MOLT TÈCNIQUES I FORMALS DES DEL PUNT DE VISTA LEGAL PERÒ SÓN DE VITAL IMPORTÀNCIA. En UTILITZAR AQUEST LLOC, Un. ACCEPTA AQUESTES CONDICIONS D'ÚS. EN CAS QUE Un. NO AQUEST D'acord AMB AQUESTES CONDICIONS D'ÚS, PER FAVOR NO UTILITZI AQUEST LLOC O ELS SERVEIS QUE EL MATEIX LI OFEREIX. GRÀCIES.</p>
                <p><b>1.</b>L'acceptació a aquests termes del servei (“Termes de Servei”) és un acord legal vinculant entre vostè respecte a l'ús del Lloc web i tots els productes o serveis disponibles del Lloc web. Si us plau, llegeixi aquests Termes de Servei acuradament. En accedir o utilitzar el Lloc web, vostè expressa el seu acord amb (1) els Termes del Servei, i (2) les Normes de la comunitat incorporades i detallades en les presents condicions generals. Si no està d'acord amb qualsevol d'aquests termes o les Normes de la comunitat, per favor, no utilitzi aquest lloc o els serveis que el mateix ofereix.</p>
                <p><b>2.</b> Actualització dels Termes del Servei. Encara que intentarem notificar als lectors quan es realitzen canvis importants a les presents Condicions de Servei, vostè ha de revisar periòdicament la versió vigent més actualitzada de les Condicions del servei. Comunitat Docent, a la seva discreció, pot modificar o revisar aquestes Condicions de servei i polítiques a qualsevol moment, i vostè accepta que quedarà vinculat per aquestes modificacions o revisions.
</p>
                <p><b>3.</b> Les presents Condicions de Servei s'apliquen a tots els usuaris del Lloc web, inclosos els usuaris que participin aportant continguts tals com a imatges, informació i altres materials o serveis en el Lloc web.</p>
                <p><b>4.</b>Comunitat Docent es reserva el dret a modificar qualsevol aspecte del Lloc web a qualsevol moment.</p>
                <p><u>Normes de la Comunitat</u></p>
                <p> <b>1.</b> L'Usuari es compromet a complir amb els termes i condicions d'aquests Termes de Servei, Normes de la comunitat, i totes les lleis locals, nacionals i reglaments internacionals.</p>
                <p><b>2.</b> L'Usuari es compromet a no fer-se passar per una altra persona o organització, la qual cosa pot constituir un delicte de suplantació d'identitat d'acord amb el Codi Penal Espanyol.</p>
                <p><b>3.</b> L'Usuari es compromet a no assetjar a qualsevol altre usuari i utilitzar un llenguatge respectuós i no ofensiu amb la resta d'Usuaris.</p>
                <p><b>4.</b> L'Usuari es compromet a no eludir, desactivar o interferir en les funcions relacionades amb la seguretat del Lloc web que impedeixin o limitin l'ús o còpia de qualsevol Contingut o fer complir les limitacions de l'ús del Lloc web o el seu Contingut en el mateix.</p>
                <p><b>5.</b> No es permet l'ús de la signatura o avatar com a mitjà de promoció o publicitat de productes, serveis, programes d'afiliats o webs, en el cas que continguin publicitat o tinguin finalitats comercials.</p>
                <p><b>6.</b> No es permet l'ús de múltiples comptes per un mateix usuari excepte casos excepcionals autoritzats per un administrador o motius justificats (múltiples usuaris en un mateix ordinador, etc). La detecció d'aquest fet pot donar lloc al bloqueig de tots els comptes associats al mateix usuari i/o bloqueig permanent d'accés.</p>
                <p><b>7.</b> L'ús d'aquesta web com a mitjà per organitzar atacs o spam a qualsevol tipus de servei (fòrums, webs, etc) no està permès. Aquest tipus de continguts podran ser eliminats i els comptes que incompleixin aquesta norma podran ser igualment cancel·lades.</p>
                <p><b>8.</b> L'administrador i moderadors del lloc web tenen el dret a esborrar, editar, moure o tancar qualsevol contingut i/o compte d'usuari que incompleixi qualsevol de les normes i obligacions descrites en aquests termes legals o pugui ser considerat inadequat per Comunitat Docent.</p>
                <p><b>9.</b> No són admissibles missatges amb amenaces, insults greus o qualsevol altre tipus de comentari que pugui ferir la sensibilitat del destinatari. En tal cas, ens reservem el dret d'avisar a les autoritats pertinents.</p>
                <p><b>10.</b> Els enviaments d'usuari ha de complir la <a href="https://support.google.com/adsense/answer/1348688" target="_blank">Política de contingut</a> de Google Adsense. Si una aportació no compleix amb la mateixa, serà eliminada.</p>      
                <p><b>11.</b> Vostè accepta que el seu comportament en el Lloc web atén a les normes de la comunitat que seran actualitzades periòdicament. Li preguem que periòdicament les consulti.</p>      
                <p><u>Propietat Intel·lectual dels Arxius d'Usuaris</u></p>
                <p><b>1.</b> Vostè pot enviar imatges i text (“Comentaris d'usuaris”) al Lloc web. Les fotos, comentaris o qualsevol altra obra o material que incorporin els usuaris es coneixen col·lectivament com a “Arxius d'Usuari”.</p>
                <p><b>2.</b> Vostè accepta que en cas de ser publicats aquests Arxius d'Usuari, seran posats a lliure disposició de la resta d'usuaris del Lloc web, sense cap limitació.</p>
                <p><b>3.</b> L'Usuari és l'únic responsable dels Arxius d'Usuari remesos i accepta les conseqüències del seu enviament al Lloc web i de la seva publicació. L'usuari afirma, i / o garanteix ser amo i/o disposar de tots els drets necessaris per a la publicació dels arxius d'Usuari en el Lloc web, autoritzant, per tant, a l'empresa per a la seva comunicació pública, ús i explotació en la forma que estimin convenient, sense cap limitació geogràfica o temporal.</p>
                <p><b>4.</b> Aquesta autorització, que, si escau, revestirà la forma legal de llicència perpètua, irrevocable, mundial, no exclusiva, gratuïta, subllicenciable i transferible per usar, reproduir, distribuir, modificar, adaptar, traduir i, sota qualsevol altra forma, explotar els Arxius d'Usuari, inclosa la promoció i redistribució de part o la totalitat del Lloc web en qualsevol format i a través de qualsevol canal de comunicació.</p>
                <p><b>5.</b> Qualsevol tercer diferent de Comunitat Docent o de les persones físiques o jurídiques expressament autoritzades per la mateixa que pretengui extreure, usar, publicar o explotar, sota qualsevol forma, els continguts generats pels usuaris, haurà de recaptar, prèviament i expressament, el consentiment dels seus titulars o, si escau, de Comunitat Docent.</p>
                <p><b>6.</b> L'Usuari es compromet a no presentar cap material que ostenti drets de propietat intel·lectual o industrial o que estiguin protegits pel secret comercial o de qualsevol altre tipus, incloent-hi la privadesa i drets de publicitat, excepte en el cas que sigui el propietari dels drets o tingui el permís del seu titular per publicar el material i concedir al Lloc web tots els drets de llicència atorgats en aquest document.</p>
                <p><u> Exclusió de responsabilitat</u></p>
                <p><b>1.</b>L'Usuari entén que en utilitzar el Lloc web, Comunitat Docent no és responsable de l'exactitud, utilitat, seguretat o drets de propietat intel·lectual d'o en relació amb els Arxius d'Usuari. L'Usuari entén i reconeix que els Arxius d'Usuaris poden resultar inexactes, ofensius i en alguns casos resultar indecents o censurables.</p>
                <p><b>2.</b>El Lloc web pot contenir enllaços a llocs web de tercers que no són propietat o no són controlades per Comunitat Docent, qui manca de control, i no assumeix cap responsabilitat pel contingut, polítiques de privadesa o pràctiques dels llocs web de tercers. A més, Comunitat Docent no pot censurar o editar el contingut de qualsevol lloc de tercers. Mitjançant l'ús del Lloc web expressament exclou a Comunitat Docent de tota i qualsevol responsabilitat que sorgeixi de l'ús de qualsevol lloc web de tercers.</p>
                <p><b>3.</b>Comunitat Docent no comparteix ni fa propis, de manera enumerativa però no limitativa, els enviaments d'usuari ni les entrades, comentaris, recomanacions, consells i opinions expressats en els Arxius d'Usuari, eximint-se Comunitat Docent de tota responsabilitat que es produeixi per la publicació en el Lloc web dels Arxius dels Usuaris.</p>
                <p><b>4.</b>El Lloc web no permet les activitats infractores dels drets d'autor en el seu Lloc web, i Comunitat Docent té la potestat d'esborrar tots els continguts i enviaments d'usuari que infringeixin aquests drets. El Lloc web es reserva el dret d'eliminar Contingut d'Usuari sense previ avís, en cas que existeixin dubtes sobre el compliment de les presents condicions d'ús.</p>
                </span>
            </div>
        </div>
    </div>
</div>
