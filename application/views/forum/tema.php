<div class="container contenido">
    <div class="row">
        <div class="col s12 m12 l12">

            <nav class="z-depth-2 breadcrumbForo breadPosts col s10">
                <div class="nav-wrapper">
                    <div class="col s12 m12 l12">
                        <?php
                        $idCategoria = $this->forum_model->listarPostsTemaParam((int) $this->uri->segment(3), "categoria_id");
                        $categoria = url_title(convert_accented_characters($this->forum_model->NombreCategoria($this->forum_model->listarPostsTemaParam((int) $this->uri->segment(3), "categoria_id"))), '-', TRUE);

                        echo anchor(BASE_URL . 'forum', 'Fòrum', array('class' => 'breadcrumb breadForum'));
                        echo anchor(BASE_URL . 'forum/' . $idCategoria . '/' . $categoria, $this->forum_model->NombreCategoria($this->forum_model->listarPostsTemaParam((int) $this->uri->segment(3), "categoria_id")), array('class' => 'breadcrumb breadForum'));

                        echo '<span class="breadcrumb"><b class="breadCategoria">' . $this->forum_model->listarPostsTemaParam((int) $this->uri->segment(3), "titulo") . '</b></span>';
                        ?>
                    </div>
                </div>
            </nav> 

            <div class="col s2 valign-wrapper">
                <?php
                if ($this->session->userdata('is_logued_in')) {
                    echo '<a class="waves-effect waves-light btn modal-trigger btnNouTema valign  blue accent-4" href="#modalTemaNuevo"><i class="material-icons left">reply</i>Respondre</a>';
                } else {
                    echo '<a class="waves-effect waves-light btn modal-trigger btnNouTema valign  blue accent-4 disabled" href="#modalSesion"><i class="material-icons left">reply</i>Respondre</a>';
                }
                ?>
            </div>

            <!-- Modal Nuevo Tema -->
            <div id="modalTemaNuevo" class="modal bottom-sheet">

                <div class="card-panel z-depth-1 col s12"> 

                    <div class="row col s3"><h4><b>Resposta</b></h4></div>
                    <div class="row col s6">

                        <ul id="menuFormularioHtml">
                            <li class="instertObject" title="Imatge"><i class="material-icons hoverable circle imagen">add_a_photo</i></li>
                            <li class="instertObject ultiObj" title="Enllaç"><i class="material-icons hoverable circle link">insert_link</i></li>
                            <li class="instertFormat" title="Negreta"><i class="material-icons hoverable circle negrita">format_bold</i></li>
                            <li class="instertFormat" title="Cursiva"><i class="material-icons hoverable circle cursiva">format_italic</i></li>
                            <li class="instertFormat ultiObj" title="Subratllat"><i class="material-icons hoverable circle subrayado">format_underlined</i></li>
                            <li class="instertList"><i class="material-icons hoverable circle lista">format_list_bulleted</i></li>
                            <li class="instertList"><i class="material-icons hoverable circle linea">linear_scale</i></li>
                        </ul> 

                        <div class="card-panel col s12 light-green lighten-5 cardTemaNuevo">
                            <form class="col s12" name="formRespostaTema" id="formRespostaTema" action="<?= BASE_URL; ?>forum/responderTema" method="post" >
                                <div class="row">
                                    <div class="input-field">                                        
                                        <textarea id="message" name="message" class="materialize-textarea"></textarea>
                                        <label for="message">Missatge</label>
                                    </div>
                                </div>
                                <input id="id_usuario" name="id_usuario" type="hidden" value="<?= $this->session->userdata('id_usuario') ?>">
                                <input id="id_tema" name="id_tema" type="hidden" value="<?= (int) $this->uri->segment(3); ?>">
                                <div class="row col s12"> 
                                    <button class="btn waves-effect waves-light blue accent-4 right" type="submit" id="respostaTema">Respondre
                                        <i class="material-icons right">send</i>
                                    </button>
                                </div>    
                            </form>
                        </div>                   

                    </div>
                    <div class="row col s3"></div>
                </div>

                <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat hide btCerrarNuevoTema">Tancar</a>

            </div>
            
                <?php
                if ($posts->num_rows() > 0) {
                    echo '<table id="listaRespuestas">
                                <tbody>';
                    foreach ($posts->result() as $post) {
                        $numero_mensajes = $this->forum_model->numeroMensajes($post->usuario_id);
                        echo '<tr><td>
                            <div class="card-panel z-depth-1 col s12 m12 l12 cardPost">
                                  <div class="card z-depth-1 col s2 m2 l2 cardUsuForo center-align">
                                    <div class="card-content activator">
                                        <img src="' . BASE_URL . 'assets/img/pers100.png" alt="" class="circle responsive-img activator hoverable" height="65" width="65"> 
                                                                    <div class="divider dividerForum"></div>
                                                                    <p card-title activator><b>' . $post->nombre . '</b></p>
                                                                    <p>';
                        echo $post->nombre_rango;
                        echo'</p>
                                    </div>
                                    <div class="card-reveal secre z-depth-5 left-align">
                                      <span class="card-title ">' . $post->nombre . '<i class="material-icons right">close</i></span>
                                      <p>Missatges:  ' . $numero_mensajes . '</p>
                                    </div>
                                  </div>
                                <div class="col s10 m10 l10 center-align">';
                        echo '<p class="right-align fechaPost">' . $post->fecha_normal . '</p>';
                        if($idCategoria === '40' && $post->archivo != NULL){
                            echo '<img src="' . BASE_URL . 'assets/img/iconDescarga.png" alt="" class="responsive-img" height="60" width="60"> ';
                            echo '<p><a href="'.BASE_URL.'files/'.$post->archivo.'" download="'.$post->archivoOriginal.'">'.$post->archivoOriginal.'</a></p>';
                        }
                        // Parsea el mensaje y hace posible los saltos de linea con nl2br.
                        $cuerpo = ParsearTags::parsear(nl2br($post->cuerpo));
                        echo '<p>' . $cuerpo . '</p></div></td></tr>';
                    }
                   echo "</tbody></table>";
                } else {
                    echo '<p>No hi ha cap tema creat.</p>';
                }
                ?>
            </div>
        </div>
    </div>
</div>
<script src="<?= BASE_URL; ?>assets/js/paging.js"></script>
<script>

    $(document).ready(function () {
        
        $('#listaRespuestas').paging({
            limit: 16,
            rowDisplayStyle: 'block',
            activePage: 1,
            rows: []
        });

        // Valida y envia el formulario de creación de un nuevo tema.
        $("#formRespostaTema").validate({
            rules: {
                message: {
                    required: true
                }
            },
            messages: {
                message: {
                    required: "Si us plau, escriu una contrasenya."
                }
            },
            errorPlacement: function (error, element) {
                error.appendTo(element.parent("div").next("span"));
            }, submitHandler: function (form) {

                var message = $('form[name=formRespostaTema] textarea[name=message]')[0].value;
                var usuario_id = $('form[name=formRespostaTema] input[name=id_usuario]')[0].value;
                var tema_id = $('form[name=formRespostaTema] input[name=id_tema]')[0].value;

                $.ajax({
                    type: "POST",
                    url: "<?= BASE_URL; ?>forum/responderTema",
                    data: {
                        message: message,
                        usuario_id: usuario_id,
                        tema_id: tema_id
                    }
                })

                        .done(function (response) {
                            if (response == true) {
                                // Cierra el modal.
                                $(".btCerrarNuevoTema").trigger("click");
                                $('#formRespostaTema')[0].reset();
                                // Recarga solamente la lista de respuestas del tema.
                                $("#listaRespuestas").load(location.href + " #listaRespuestas>*", "");
                            } else {
                                alert(response);
                            }
                        });
                return false;
            }
        });


    });

</script>