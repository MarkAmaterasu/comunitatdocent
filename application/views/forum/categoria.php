<div class="container contenido">
    <div class="row">
        <div class="col s12 m12 l12">

            <nav class="z-depth-2 breadcrumbForo col s10">
                <div class="nav-wrapper">
                    <div class="col s12">
                        <?php
                        echo anchor(BASE_URL . 'forum', 'Fòrum', array('class' => 'breadcrumb breadForum'));

                        echo '<span class="breadcrumb"><b class="breadCategoria">' . $this->forum_model->nombreCategoria((int) $this->uri->segment(2)) . '</b></span>';
                        ?>
                    </div>
                </div>
            </nav>
            <div class="col s2 valign-wrapper">
                <?php
                // No se porque lo puse, parece que no hace nada.
                /* $categoriaNuevoTema = url_title(convert_accented_characters($this->forum_model->nombreCategoria((int) $this->uri->segment(2))), '-', TRUE);
                  $urlNuevoTema = 'forum/' . $this->uri->segment(2) . '/' . $categoriaNuevoTema . '/';
                  $urlNuevoTema .= url_title(convert_accented_characters("nou tema"), '-', TRUE); */
                if ($this->session->userdata('is_logued_in')) {
                    echo '<a class="waves-effect waves-light btn modal-trigger btnNouTema valign  blue accent-4" href="#modalTemaNuevo"><i class="material-icons left">note_add</i>Nou Tema</a>';
                } else {
                    echo '<a class="waves-effect waves-light btn modal-trigger btnNouTema valign  blue accent-4 disabled" href="#modalSesion"><i class="material-icons left">note_add</i>Nou Tema</a>';
                }
                ?>
            </div>

            <!-- Modal Nuevo Tema -->
            <div id="modalTemaNuevo" class="modal bottom-sheet">

                <div class="card-panel z-depth-1 col s12"> 

                    <div class="row col s3"><h4><b>Nou tema</b></h4></div>
                    <div class="row col s6">

                        <ul id="menuFormularioHtml">
                            <li class="instertObject" title="Imatge"><i class="material-icons hoverable circle imagen">add_a_photo</i></li>
                            <li class="instertObject ultiObj" title="Enllaç"><i class="material-icons hoverable circle link">insert_link</i></li>
                            <li class="instertFormat" title="Negreta"><i class="material-icons hoverable circle negrita">format_bold</i></li>
                            <li class="instertFormat" title="Cursiva"><i class="material-icons hoverable circle cursiva">format_italic</i></li>
                            <li class="instertFormat ultiObj" title="Subratllat"><i class="material-icons hoverable circle subrayado">format_underlined</i></li>
                            <li class="instertList"><i class="material-icons hoverable circle lista">format_list_bulleted</i></li>
                            <li class="instertList"><i class="material-icons hoverable circle linea">linear_scale</i></li>
                        </ul> 

                        <div class="card-panel col s12 light-green lighten-5 cardTemaNuevo">
                            <?php if ((int) $this->uri->segment(2) != 40) { ?>
                                <form class="col s12" name="formNouTema" id="formNouTema" action="<?= BASE_URL; ?>forum/tema_nuevo" method="post" >
                                    <div class="row ">
                                        <div class="input-field">
                                            <input id="titulo" name="titulo" type="text">
                                            <label for="titulo">Títol</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field">                                        
                                            <textarea id="message" name="message" class="materialize-textarea"></textarea>
                                            <label for="message">Missatge</label>
                                        </div>
                                    </div>
                                    <input id="id_usuario" name="id_usuario" type="hidden" value="<?= $this->session->userdata('id_usuario') ?>">
                                    <input id="id_categoria" name="id_categoria" type="hidden" value="<?= (int) $this->uri->segment(2); ?>">
                                    <div class="row col s12"> 
                                        <button class="btn waves-effect waves-light blue accent-4 right" type="submit" id="nouTema">Publicar tema
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </div>    
                                </form>
                            <?php } else { ?>
                                <form class="col s12" name="formNouTemaFitxer" id="formNouTemaFitxer" action="<?= BASE_URL; ?>forum/tema_nuevo_archivo" method="post" enctype="multipart/form-data">
                                    <div class="row ">
                                        <div class="input-field">
                                            <input id="titulo" name="titulo" type="text">
                                            <label for="titulo">Títol</label>
                                        </div>
                                    </div>
                                    <input type="hidden" name="MAX_FILE_SIZE" value="20971520">
                                    <div class="row ">
                                        <div class="file-field input-field">
                                            <div class="btn">
                                                <span>Fitxer</span>
                                                <input id="archivo" name="archivo" type="file">                                                  
                                            </div>                                                
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" id="archivo" name="archivo" placeholder="Arxiu tipus PDF, màxim 19MB">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field">                                        
                                            <textarea id="message" name="message" class="materialize-textarea"></textarea>
                                            <label for="message">Missatge</label>
                                        </div>
                                    </div>
                                    <input id="id_usuario" name="id_usuario" type="hidden" value="<?= $this->session->userdata('id_usuario') ?>">
                                    <input id="id_categoria" name="id_categoria" type="hidden" value="<?= (int) $this->uri->segment(2); ?>">
                                    <div class="row col s12"> 
                                        <button class="btn waves-effect waves-light blue accent-4 right btnouTemaFitxer" type="submit" id="btnouTemaFitxer">Publicar tema
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </div>    
                                </form>
                                <!--div para visualizar mensajes-->
                                <div class="messages"></div><br /><br />
                            <?php } ?>
                        </div>                   

                    </div>
                    <div class="row col s3"></div>
                </div>

                <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat hide btCerrarNuevoTema">Tancar</a>

            </div>



            <?php
            if ($temas->num_rows() > 0) {
                echo '<div class="card-panel z-depth-1 col s12" id="tablaTemas"> 
                <table class="striped tablaTemas" id="idTabla">';
                echo '<thead>
                            <tr>
                                <th data-field="nomForum" class="col s5 m5 l5">Tema</th>
                                <th data-field="ultimTema" class="col s4 m4 l4 center-align">Autor</th>
                                <th data-field="nombreTemes" class="col s2 m2 l2 center-align">Últim missatge</th>
                                <th data-field="nombreTemes" class="col s1 m1 l1 center-align">Respostes</th>                                
                            </tr>
                          </thead>
                          <tbody>';

                foreach ($temas->result() as $post) {
                    $categoria = url_title(convert_accented_characters($this->forum_model->nombreCategoria((int) $this->uri->segment(2))), '-', TRUE);
                    $url = 'forum/' . $categoria . '/' . $post->id . '/';
                    $url .= url_title(convert_accented_characters($post->titulo), '-', TRUE);
                    echo '<tr class="filaCategoriaForo"><td class="col s5 m5 l5 alturaT">' . anchor($url, $post->titulo) . '</td>';
                    echo '<td class="col s4 m4 l4 center-align">' . anchor($url, $post->nombre);
                    echo '<p class = "descripcionCategoria">' . $post->fecha . '</p></td>';

                    $tema_id = $post->id;
                    $idCategoria = (int) $this->uri->segment(2);
                    $ultimas_respuestas = $this->forum_model->ultima_respuesta($idCategoria, $tema_id);
                    foreach ($ultimas_respuestas->result() as $ultima_respuesta) {
                        if ($ultima_respuesta->nombreUsuario == NULL) {
                            echo '<td class="col s2 m2 l2 center-align"></td>';
                        } else {
                            echo '<td class="col s2 m2 l2 center-align">' . $ultima_respuesta->nombreUsuario;
                            echo '<p class = "descripcionCategoria">' . $ultima_respuesta->fechaEU . '</p></td>';
                        }
                    }
                    $numero_respuestas = $this->forum_model->numero_respuestas($idCategoria, $tema_id);
                    foreach ($numero_respuestas->result() as $numero) {
                        if ($numero->numero_respuestas == -1) {
                            echo '<td class="col s1 m1 l1 center-align">0</td>';
                        } else {
                            echo '<td class="col s1 m1 l1 center-align">' . $numero->numero_respuestas;
                            echo '</td>';
                        }
                    }

                    echo '</tr>';
                }
                echo '</tbody>
                          </table>
                    </div>';
            } else {
                echo '<p>No hi ha cap  tema creat.</p>';
            }
            ?>

        </div>
    </div>
</div>
<!-- Se carga el JS de la paginación de la tabla -->
<script src="<?= BASE_URL; ?>assets/js/paging.js"></script>
<script>

            $(document).ready(function () {
    // Paginación de la tabla.
    $('#idTabla').paging({
    limit: 20,
            rowDisplayStyle: 'block',
            activePage: 1,
            rows: []
    });
    ///////////////////////////////////////////////////////////////
            // Valida y envia el formulario de creación de un nuevo tema.
            ///////////////////////////////////////////////////
            $("#formNouTema").validate({
    rules: {
    titulo: {
    required: true
    },
            message: {
            required: true
            }
    },
            messages: {
            titulo: {
            required: "Si us plau, escriu un títol."
            },
                    message: {
                    required: "Si us plau, escriu el missatge."
                    }
            },
            errorPlacement: function (error, element) {
            error.appendTo(element.parent("div").next("span"));
            }, submitHandler: function (form) {

    var titulo = $('form[name=formNouTema] input[name=titulo]')[0].value;
            var message = $('form[name=formNouTema] textarea[name=message]')[0].value;
            var usuario_id = $('form[name=formNouTema] input[name=id_usuario]')[0].value;
            var categoria_id = $('form[name=formNouTema] input[name=id_categoria]')[0].value;
            $.ajax({
            type: "POST",
                    url: "<?= BASE_URL; ?>forum/tema_nuevo",
                    data: {
                             titulo: titulo,
                            message: message,
                            usuario_id: usuario_id,
                            categoria_id: categoria_id
                    }
            })

            .done(function (response) {
            if (response == true) {
            // Cierra el modal.
            $(".btCerrarNuevoTema").trigger("click");
            $('#formNouTema')[0].reset();
                    // Recarga solamente la tabla de datos.
                    $("#tablaTemas").load(location.href + " #tablaTemas>*", "");
            } else {
            alert(response);
            }
            });
            return false;
    }
    });
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    
            $(".messages").hide();
//queremos que esta variable sea global
            var fileExtension = "";
//función que observa los cambios del campo file y obtiene información
            $(':file').change(function()
    {
    //obtenemos un array con los datos del archivo
    var file = $("#archivo")[0].files[0];
            //obtenemos el nombre del archivo
            var fileName = file.name;
            //obtenemos la extensión del archivo
            fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
            //obtenemos el tamaño del archivo
            var fileSize = file.size;
            //obtenemos el tipo de archivo image/png ejemplo
            var fileType = file.type;
            //mensaje con la información del archivo
            if(fileSize >= 1000000){
                 showMessage("Solo se pueden subir archivos de hasta 1MB");
                 $( "#btnouTemaFitxer" ).addClass( "disabled" );
                 
                 $("button:submit").click(function() { return false; });
                 
            }else{
                 showMessage("<span class='info'>Archivo para subir: " + fileName + ", peso total: " + fileSize + " bytes.</span>");
                 $( ".btnouTemaFitxer" ).removeClass( "disabled" );
                    
            }
    });            
                ///////////////////////////////////////////////////////////////
            // Valida y envia el formulario de creación de un nuevo tema.
            ///////////////////////////////////////////////////
            $("#formNouTemaFitxer").validate({
    rules: {
    titulo: {
    required: true
    },
            message: {
            required: true
            }
    },
            messages: {
            titulo: {
            required: "Si us plau, escriu un títol."
            },
                    message: {
                    required: "Si us plau, escriu el missatge."
                    }
            },
            errorPlacement: function (error, element) {
            error.appendTo(element.parent("div").next("span"));
            }, submitHandler: function (form) {
                
            var formData = new FormData($("#formNouTemaFitxer")[0]); 

            $.ajax({
            type: "POST",
                    url: "<?= BASE_URL; ?>forum/tema_nuevo_archivo",
                    data: formData,
                    //necesario para subir archivos via ajax
                    cache: false,
                    contentType: false,
                    processData: false
            })
            .done(function (response) {
            if (response == true) {
                // Cierra el modal.
                $(".btCerrarNuevoTema").trigger("click");
                $('#formNouTemaFitxer')[0].reset();
                // Recarga solamente la tabla de datos.
                $("#tablaTemas").load(location.href + " #tablaTemas>*", "");
            } else {
                alert(response);
            }
            });
            return false;
    }
    });


//como la utilizamos demasiadas veces, creamos una función para 
//evitar repetición de código
            function showMessage(message){
            $(".messages").html("").show();
                    $(".messages").html(message);
            }




    });

</script>