<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="ca">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="description" content="Pàgina pensada per ayudar als professors y que colaborin entre ells.">
        <meta name="author" content="Marc Trallero García">
        <meta name="keywords" content="HTML5,CSS3,JQuery,MaterializeCSS,PHP">

        <title><?php echo $titulo?></title>

        <!-- icono web-->
        <link rel="shortcut icon" href="<?= BASE_URL; ?>assets/img/logotipo/ComIcon.png">

        <!--    Se cargan los estilos de materialize -->
        <link rel="stylesheet" href="<?= BASE_URL; ?>assets/css/materialize/css/materialize.min.css">
        <!-- Se cargan los iconos  -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">   
        <!-- Se cargan los estilos --> 
        <link rel="stylesheet" href="<?= BASE_URL; ?>assets/css/estilos.css">
        <link rel="stylesheet" href="<?= BASE_URL; ?>assets/css/cookies.css">
        <!-- Se cargan el Jquery -->
        <script src="<?= BASE_URL; ?>assets/js/jquery-2.1.4.min.js"></script>
        <script src="<?= BASE_URL; ?>assets/js/jquery-ui-1.11.4/jquery-ui.min.js"></script>
        <!-- Se cargan el JS de Materialize   -->
        <script src="<?= BASE_URL; ?>assets/css/materialize/js/materialize.min.js"></script>
        <!-- Se carga la librería de Jquery Validation -->
        <script src="<?= BASE_URL; ?>assets/js/jqueryValidation/dist/jquery.validate.js" type="text/javascript"></script>
        <script src="<?= BASE_URL; ?>assets/js/jquery.form.js" type="text/javascript"></script>
        <!-- Se carga el JS -->
        <script src="<?= BASE_URL; ?>assets/js/comunitatDocent.js" type="text/javascript"></script>

    </head>
    <body>
        <header>
            <!-- Nav superior  -->
            <nav class="nav1">
                <div class="nav-wrapper">
                    <a href="<?php echo BASE_URL; ?>inici" class="brand-logo"><img class="left" src="<?= BASE_URL; ?>assets/img/logotipo/ComIcon.png" height="65" width="65"><span class="hide-on-small-only">Comunitat Docent</span></a>
                    <!--     Botón para sacar el menú lateral móbil     -->
                    <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                    <!--          Menú principal-->
                    <div class="brand-logo center hide-on-med-and-down">
                        <form>
                            <div class="input-field">
                                <input id="buscar" type="search" placeholder="Cercar" required>
                                <label for="buscar"><i class="material-icons">search</i></label>
                                <i class="mdi-navigation-close close"></i>
                            </div>
                        </form>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li>
                            <a class="waves-effect waves-light modal-trigger" href="#modalSesion" id="conectar"><i class="tiny material-icons left">perm_identity</i>Iniciar sessió</a>
                        </li>
                        <li>
                            <a class="waves-effect waves-light modal-trigger" href="#modalRegistro">Registrar-se</a>
                        </li>
                        <li>
                            <a href="<?php echo BASE_URL; ?>contacte"><i class="tiny material-icons left">info</i>Contacte</a>
                        </li>
                        <li>
                            <a href="<?php echo BASE_URL; ?>qui-som">Qui Som</a>
                        </li>
                    </ul>
                    <!--          -->
                    <!--    Menú lateral móbil      -->
                    <!--         -->
                    <ul class="side-nav center" id="mobile-demo">
                        <li>
                            <form>
                                <div class="input-field">
                                    <input id="buscarMobil" type="search" placeholder="Cercar" required>
                                    <label for="buscarMobil"><i class="material-icons">search</i></label>
                                </div>
                            </form>
                        </li>
                        <li class="divider"></li>
                        <i class="material-icons">perm_identity</i>
                        <li><a class="waves-effect waves-light modal-trigger" href="#modalSesion">Iniciar sessió</a></li>
                        <li><a class="waves-effect waves-light modal-trigger" href="#modalRegistro">Registrar-se</a></li>
                        <li class="divider"></li>
                        <i class="material-icons">info</i>
                        <li>
                            <a href="<?php echo BASE_URL; ?>contacte">Contacte</a>
                        </li>
                        <li>
                            <a href="<?php echo BASE_URL; ?>qui-som">Qui Som</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo BASE_URL; ?>inici">Inici</a></li>
                        <li><a href="">Noticies</a></li>
                        <li><a href="">Consells</a></li>
                        <li><a href="">Recursos</a></li>
                        <li class="divider"></li>
                        <li><a href="">Infantil</a></li>
                        <li><a href="">Primària</a></li>
                        <li><a href="">ESO</a></li>
                        <li><a href="">Batxillerat</a></li>
                        <li><a href="">Cicles de Formació</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo BASE_URL; ?>forum">Fòrum</a></li>
                    </ul>
                </div>
            </nav>
            <!--      Nav inferior-->
            <nav class="hide-on-med-and-down z-depth-3 nav2">
                <div class="nav-wrapper container">
                    <ul class="left general">
                        <li><a href="<?php echo BASE_URL; ?>inici">Inici</a></li>
                        <li><a href="">Noticies</a></li>
                        <li><a href="">Consells</a></li>
                        <li><a href="">Recursos</a></li>
                    </ul>
                    <ul class="left etapas truncate">
                        <li><a href="">Infantil</a></li>
                        <li><a href="">Primària</a></li>
                        <li><a href="">ESO</a></li>
                        <li><a href="">Batxillerat</a></li>
                        <li><a href="">Cicles de Formació</a></li>
                    </ul>
                    <ul class="dropdown-content blue lighten-2" id="dropEtapas">
                        <li><a href="" class="black-text">Infantil</a></li>
                        <li><a href="" class="black-text">Primària</a></li>
                        <li><a href="" class="black-text">ESO</a></li>
                        <li><a href="" class="black-text">Batxillerat</a></li>
                        <li><a href="" class="black-text">Cicles de Formació</a></li>
                    </ul>
                    <ul class="left dropEtapasBt">
                     <li><a class="dropdown-button" href="#!" data-activates="dropEtapas">Etapes<i class="material-icons right">arrow_drop_down</i></a></li>
                    </ul>
                    <ul class="left foro">
                        <li><a href="<?php echo BASE_URL; ?>forum">Fòrum</a></li>
                    </ul>
                </div>
            </nav>
            <!--  Modal inicio de sesión -->
            <div id="modalSesion" class="modal">
                <div class="modal-content">
                    <div class="row">
                        <form class="col s12" id="formInicio" name="formInicio" action="<?= BASE_URL; ?>usuario/login" method="post">
                            <div>
                                <h4 class="center">Iniciar Sessió</h4>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">email</i>
                                    <input id="emailSesion" name="emailSesion" type="email" class="validate" required>
                                    <label for="emailSesion">Correu Electrònic</label>
                                </div>
                                <span id="errorEmailSesion" name="errorEmailSesion"></span>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">vpn_key</i>
                                    <input id="passwordSesion" name="passwordSesion" type="password" class="validate" required>
                                    <label for="passwordSesion">Contrasenya</label>
                                </div>
                                <span id="errorPass" name="errorPass"></span>
                            </div>
                            <input name="token" id="token" value="<?= $token ?>" type="hidden">
                            <button class="btn waves-effect waves-light" type="submit" name="botonSesion" id="botonSesion">Entrar
                                <i class="material-icons right">send</i>
                            </button>
                            <div class="row">
                                <div class="col s12">
                                    <p>No tens un compte? <a class="waves-effect waves-light modal-trigger noCuenta" href="#modalRegistro">Registrar-se</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat cerrarModalInicio">Tancar</a>
                </div>
            </div>
            <!--     Modal de registro -->
            <div id="modalRegistro" class="modal">
                <div class="modal-content">
                    <div class="row">
                        <form class="col s12" id="formRegistro" name="formRegistro" action="<?= BASE_URL; ?>usuario/registro" method="post">
                            <div>
                                <h4 class="center">Registra't</h4>
                            </div>
                            <input name="tokenRegistro" id="tokenRegistro" value="<?= $token ?>" type="hidden">
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">assignment_ind</i>
                                    <input id="nombreRegistro" name="nombreRegistro" type="text" class="validate">
                                    <label for="nombreRegistro">Nom</label>
                                </div>
                                <span id="errorNombre" name="errorNombre"></span>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">email</i>
                                    <input id="emailRegistro" name="emailRegistro" type="email" class="validate">
                                    <label for="emailRegistro">Correu Electrònic</label>
                                </div>
                                <span id="errorEmail" name="errorEmail"></span>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">vpn_key</i>
                                    <input id="passwordRegistro" name="passwordRegistro" type="password" class="validate">
                                    <label for="passwordRegistro">Contrasenya</label>
                                </div>
                                <span id="errorContrasenya" name="errorContrasenya"></span>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">vpn_key</i>
                                    <input id="passwordRegistro2" name="passwordRegistro2" type="password" class="validate">
                                    <label for="passwordRegistro2">Confirma la contrasenya</label>
                                </div>
                                <span id="errorContrasenya2" name="errorContrasenya2"></span>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input type="checkbox" id="terminos" name="terminos"><label for="terminos">He llegit y accepto la <a href="<?= BASE_URL; ?>privacitat">política de privacitat i de participació</a>.</label>
                                </div>
                                <span id="errorTermino" name="errorTermino" class="col s6 offset-s6"></span>
                            </div>
                            <button class="btn waves-effect waves-light col s2 offset-s10" type="submit" id="botonRegistro" name="botonRegistro">Registrar-se
                                <i class="material-icons right">send</i>
                            </button>
                            <div class="row">
                                <div class="col s6">
                                    <p>Ja tens un compte? <a class="waves-effect waves-light modal-trigger siCuenta" href="#modalSesion">Entra</a></p>
                                </div>                 
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#" class=" modal-action modal-close waves-effect waves-green btn-flat" id="cerrarModalRegistro">Tancar</a>
                </div>
            </div>
        </header>
        <main>