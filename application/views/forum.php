<div class="container contenido">
    <div class="row">
        <div class="col s12 m12 l12">

            <?php
            // Lista las categorias del foro.
            if ($categorias->num_rows() > 0) {
                foreach ($categorias->result() as $categoria) {
                    echo '<div class="card-panel z-depth-2 cardForum"> 
                <table class="striped">';
                    echo '<h5><b>' . $categoria->nombre . '</b></h5>';
                    echo '<div class="divider dividerForum"></div>';
                    echo '<thead>
                            <tr>
                                <th data-field="nomForum" class="col s5 m5 l5">Fòrum</th>
                                <th data-field="ultimTema" class="col s5 m5 l5 center-align">Últim tema</th>
                                <th data-field="nombreTemes" class="col s2 m2 l2 center-align">Temes</th>
                            </tr>
                          </thead>
                          <tbody>';
                    // Lista las subcategorias del foro.
                    $subCategorias = $this->forum_model->listarCategorias($categoria->id);
                    foreach ($subCategorias->result() as $subCategoria) {
                        $url = 'forum/' . $subCategoria->id . '/';
                        // Cambia espacios por guiones, mayus por minus y les quita los accentos a los carácteres acentuados.
                        $url .= url_title(convert_accented_characters($subCategoria->nombre), '-', TRUE);
                        echo '<tr><td class="col s5 m5 l5"><div>' . anchor($url, $subCategoria->nombre);
                        echo  '<p class = "descripcionCategoria">'. $subCategoria->descripcion .'</p>';
                        echo '<p class = "moderadores"><b>Moderadors:</b>'.'</div></td>';

                        // Muestra el último tema publicado por subcategoria.
                        $ultimo_tema = $this->forum_model->ultimoTema($subCategoria->id);
                        foreach ($ultimo_tema->result() as $tema) {
                            $urlTema = 'forum/' . url_title(convert_accented_characters($subCategoria->nombre), '-', TRUE) . '/' . $tema->id . '/';
                            $urlTema .= url_title(convert_accented_characters($tema->titulo), '-', TRUE);
                            echo '<td class="col s5 m5 l5 alturaP center-align">' . anchor($urlTema, $tema->titulo) . '</td>';

                            // Muestra el número de temas / post de la subcategoria.
                            $numero_temas = $this->forum_model->numeroTemas($subCategoria->id);
                            echo '<td class="col s2 m2 l2 alturaP center-align">' . $numero_temas->numeroTemas . '</td></tr>';
                        }
                    }
                    echo '</tbody>
                          </table>
                    </div>';
                }
            } else {
                echo '<p>No hi ha cap categoria.</p>';
            }
            ?>
            </table>
        </div>
    </div>
</div>
</div>

