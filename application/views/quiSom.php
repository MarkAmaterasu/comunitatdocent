<div class="container contenido">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel z-depth-2">              
                <span class="black-text flow-text">
                    <p>
                        <b>Comunitat Docent</b> és un lloc web construït per dos alumnes de l'Institut Baix Camp perquè els professors i professores del nostre sistema educatiu s'ajudin entre ells.
                    </p>
                    <p>
                        Aquí podran compartir material per ensenyar, comentar els seus dubtes, aconsellar als altres i explicar les seves experiències a classe.
                    </p>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col s12 m12 l12 ">
            <div class="nota flow-text black-text valign-wrapper z-depth-3">
                <span class="citaQuienSomos"><blockquote>De professors per a professors.</blockquote></span>
            </div>            
        </div>  
    </div>  
    <div class="row">
        <div class="col s12 m12 l12">
            <p><h2 class="center">L'equip</h2></p>
            <div class="card-panel z-depth-2">              
                <div class="row valign-wrapper">
                    <div class="col s2">
                        <img  class="fotoPersonal" src="<?= BASE_URL; ?>assets/img/pers100.png " alt="" class="responsive-img">
                    </div>
                    <div class="col s10">
                        <span class="black-text flow-text">
                            Marc Trallero García.
                        </span>
                    </div>
                </div>  
            </div>  
        </div>  
    </div>  
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="nota flow-text black-text valign-wrapper z-depth-3">
                <span class="citaQuienSomos2"><blockquote>Perquè és la comunitat més important per al nostre futur.</blockquote></span>
            </div>            
        </div>  
    </div>  
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel z-depth-2">
                <div class="row valign-wrapper">
                    <div class="col s10">
                        <span class="black-text flow-text">
                            Dani Castillo.
                    </div>
                    <div class="col s2">
                        <img src="<?= BASE_URL; ?>assets/img/pers100.png " alt="" class="circle responsive-img" height="300" width="100">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
