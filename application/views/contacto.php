<div class="container contenido">
    <div class="row">
        <div class="col s6 m16 l6">
            <div class="card-panel z-depth-3">   
                
                <?php echo validation_errors(); ?>  
                
                <form action="<?php echo BASE_URL; ?>contacto/enviarEmailContacto" method="post" name="formContacto" class="formContacto" id="formContacto">
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">person_pin</i>
                            <input id="nombre" type="text" name="nombre" />
                            <label for="nombre">Noms i cognoms: </label>                        
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">email</i>
                            <label for="email">Correu electrònic: </label>
                            <input id="email" type="email" name="email" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">assignment</i>
                            <label for="asunto">Assumpte:</label>
                            <input id="asunto" type="text" class="asunto" name="asunto" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <i class="material-icons prefix">mode_edit</i>
                            <label for="mensaje">Missatge:</label>
                            <textarea id="mensaje" name="mensaje" rows=8 cols=45 class="materialize-textarea"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field center">
                            <button id="contacto" type="submit" class="waves-effect waves-light btn blue">Enviar<i class="material-icons right">send</i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col s6 m16 l6">
            <div class="card-panel">   
                <ol id="listaContacto" style="list-style-image: url(<?= BASE_URL?>assets/img/flechaDerecha.svg)">
                    <p>Tot s'arregla parlant, posa't en contacte amb nosaltres omplint el formulari sempre que tinguis:</p>
                    <li>Algun dubte sobre el funcionament.</li>
                    <li>Problemes.</li> 
                    <li>Algun suggeriment.</li>
                    <li>Etc.</li>
                    <p>També pots utilitzar el fòrum, així la comunitat també et podrà ajudar!</p>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- Script de envio del formulario de contacto mediante Ajax/Jquery -->
<script>
    /* $(document).ready(function(){                     
     
     // Al hacer click al boton siguiente, ejecuta la función definida dentro de ella.                     
     $('#contacto').click(function(e){      
     e.preventDefault();
     contacto();
     });
     
     // Función que se encarga, mediante ajax, del envio del mensaje.
     function contacto(){
     
     var nombre = $('form[name=formContacto] input[name=nombre]')[0].value;      
     var email = $('form[name=formContacto] input[name=email]')[0].value;
     var mensaje = $('form[name=formContacto] textarea[name=mensaje]')[0].value;
     
     $.ajax({
     type: "POST",
     url: "<?php // echo BASE_URL; ?>/envioContacto",
     data: 
     {   
     nombre: nombre, 
     email: email, 
     mensaje: mensaje
     }
     })
     
     .done(function(){
     alert("¡Envio efectuado con éxito!");  
     })
     
     .fail(function(){
     alert("¡Error en el envio!");
     })                        
     }
     });*/
</script>