<div class="container contenido">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel z-depth-3">              
                <span class="black-text flow-text">
                    <h2 class="center-align">Política de cookies </h2>                    
                    <p>En aquest lloc es pot utilitzar cookies quan l'usuari navega per les diferents pàgines del lloc. 
                        Durant l'ús de la nostra pàgina web acceptes i autoritzes expressament l'ús de cookies.</p>
                    <p><u>Què són les cookies?</u></p>
                    <p>Una cookie és un fitxer molt petit que es descarrega en l'ordinador/smartphone/tablet de l'usuari en accedir a determinades pàgines web per 
                        emmagatzemar i recuperar informació sobre la navegació que s'efectua des d'aquest equip.</p>
                    <p><u>Per a què serveixen les cookies?</u></p>
                    <p>La utilització de les cookies té com a finalitat exclusiva recordar les preferències de l'usuari (idioma, país, inici de sessió, característiques del seu navegador, informació d'ús de la nostra Web, etc.)</p>
                    <p>Poden ser utilitzades per obtenir informació sobre el tràfic dins del mateix lloc i estimar el nombre de visites realitzades.</p>
                    <p>Normalment els llocs web utilitzen les cookies per obtenir informació estadística sobre les seves pàgines Web. Tingui en compte que recollim dades sobre els seus moviments i ús de la nostra Web com a dades estadístiques, no personals.</p>
                    <p><u>Salvaguardes de protecció</u></p>
                    <p>L'usuari pot configurar el seu navegador per acceptar, o no, les cookies que rep o perquè el navegador li avisi quan un servidor vulgui guardar una cookie o esborrar-les del seu ordinador. Pot trobar les instruccions en la configuració de seguretat en el seu navegador Web.</p>
                    <p>Pot fer ús de la secció “Ajuda” que trobarà en la barra d'eines de la majoria de navegadors per canviar els ajustos del seu ordinador, no obstant això, algunes de les característiques dels nostres serveis online poden no funcionar o poden resultar més complicades d'accedir si rebutja totes les cookies.</p>
                    <p>Molts navegadors permeten activar una manera privada mitjançant el qual les cookies s'esborren sempre després de la seva visita.</p>
                    <p>Si us plau, llegeixi atentament la secció d'ajuda del seu navegador per conèixer més sobre com activar la “manera privada”. Podrà seguir visitant la nostra Web encara que el seu navegador estigui en “manera privada”, no obstant això, l'experiència d'usuari pot no ser òptima i algunes utilitats poden no funcionar.</p>
                </span>
            </div>
        </div>
    </div>
</div>
