<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Privacidad extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->library(array('session'));
    }
    
    public function index() {
        //  Hace un switch para saber si existe la sesión, y en ese caso saber que contiene para poder redirigir a cada usuario a la página correspondiente.
        switch ($this->session->userdata('rol')) {
            case '':
                $data['token'] = $this->token();  
                $data['titulo'] = 'Privacitat';
                $this->load->view('header', $data);                
                $this->load->view('privacidad');
                $this->load->view('footer');
                break;
            case 'Administrador':
                //redirect(base_url() . 'admin');
                $data['titulo'] = 'Privacitat';
                $this->load->view('headerSession',$data);
                $this->load->view('privacidad');
                $this->load->view('footer');
                break;
            case 'Moderador':
                //redirect(base_url() . 'jefe');
                break;
            case 'Comun':
                $data['titulo'] = 'Privacitat';
                $this->load->view('headerSession',$data);
                $this->load->view('privacidad');
                $this->load->view('footer');
                //redirect(base_url() . 'empleado');
                break;
            default:
                $data['token'] = $this->token();  
                $data['titulo'] = 'Privacitat';
                $this->load->view('header');                
                $this->load->view('privacidad', $data);
                $this->load->view('footer');
                break;
        }
    }

            // Crea una nueva clave aleatoria que será la que contendrá nuestro formulario, de esta forma se evita el Cross-Site Request Forgery. 
    public function token() {
        $token = md5(uniqid(rand(), true));
        $this->session->set_userdata('token', $token);
        return $token;
    }
    
}
