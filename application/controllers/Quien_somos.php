<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Quien_Somos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('url'));
    }

    public function index() {
        //  Hace un switch para saber si existe la sesión, y en ese caso saber que contiene para poder redirigir a cada usuario a la página correspondiente.
        switch ($this->session->userdata('rol')) {
            case '':
                $data['token'] = $this->token();
                $data['titulo'] = 'Qui som';
                $this->load->view('header', $data);
                $this->load->view('quiSom');
                $this->load->view('footer');
                break;
            case 'Administrador':                
                $data['titulo'] = 'Qui som';
                $this->load->view('headerSession', $data);
                $this->load->view('quiSom');
                $this->load->view('footer');
                break;
            case 'Moderador':
                
                break;
            case 'Comun':
                $data['titulo'] = 'Qui som';
                $this->load->view('headerSession', $data);
                $this->load->view('quiSom');
                $this->load->view('footer');                
                break;
            default:
                $data['token'] = $this->token();
                $data['titulo'] = 'Qui som';
                $this->load->view('header');
                $this->load->view('quiSom', $data);
                $this->load->view('footer');
                break;
        }
    }
    
        // Crea una nueva clave aleatoria que será la que contendrá nuestro formulario, de esta forma se evita el Cross-Site Request Forgery. 
    public function token() {
        $token = md5(uniqid(rand(), true));
        $this->session->set_userdata('token', $token);
        return $token;
    }

}
