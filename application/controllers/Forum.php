<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Forum extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'form_validation', 'cifrado'));
        $this->load->helper(array('form', 'security'));
        $this->load->model('forum_model');
    }

    public function index() {
        // Comprueba que exista una sesión.
        if ($this->session->userdata('is_logued_in')) {
            $data['categorias'] = $this->forum_model->listarCategorias();
            $data['titulo'] = 'Fòrum';
            $this->load->view('headerSession', $data);
            $this->load->view('forum');
            $this->load->view('footer');
        } else {
            $data['categorias'] = $this->forum_model->listarCategorias();
            $data['token'] = $this->token();
            $data['titulo'] = 'Fòrum';
            $this->load->view('header', $data);
            $this->load->view('forum');
            $this->load->view('footer');
        }
    }

    function categoria($id = 0) {
        if ((int) $id <= 0) {
            echo "Error, no existe una categoría con esta id.";
        } else {
            // Comprueba que exista una sesión.
            if ($this->session->userdata('is_logued_in')) {
                $data['temas'] = $this->forum_model->listarTemas($id);
                $data['titulo'] = 'Fòrum';
                $this->load->view('headerSession', $data);
                $this->load->view('forum/categoria');
                $this->load->view('footer');
            } else {
                $data['temas'] = $this->forum_model->listarTemas($id);
                $data['titulo'] = 'Fòrum';
                $data['token'] = $this->token();
                $this->load->view('header', $data);
                $this->load->view('forum/categoria');
                $this->load->view('footer');
            }
        }
    }

    function post($id = 0) {
        if ((int) $id <= 0) {
            echo "Error, no existe un tema con esta id.";
        } else {
            if ($this->session->userdata('is_logued_in')) {
                $data['posts'] = $this->forum_model->listarPosts($id);
                $data['titulo'] = 'Fòrum';
                $this->load->view('headerSession', $data);
                $this->load->view("forum/tema");
                $this->load->view('footer');
            } else {
                $data['posts'] = $this->forum_model->listarPosts($id);
                $data['titulo'] = 'Fòrum';
                $data['token'] = $this->token();
                $this->load->view('header', $data);
                $this->load->view("forum/tema");
                $this->load->view('footer');
            }
        }
    }

    // Crea una nueva clave aleatoria que será la que contendrá nuestro formulario, de esta forma se evita el Cross-Site Request Forgery. 
    public function token() {
        $token = md5(uniqid(rand(), true));
        $this->session->set_userdata('token', $token);
        return $token;
    }

    ////////////////////////////////  MÉTODOS ///////////////////////////////////////////////////
    // Registra un tema nuevo.
    public function tema_nuevo() {

        $this->form_validation->set_rules('titulo', 'titulotema', 'required|trim|xss_clean|htmlentities');
        $this->form_validation->set_rules('message', 'mentema', 'required|trim|xss_clean|htmlentities');

        // Si la validación es inválida, devolverá error. 
        if ($this->form_validation->run() == FALSE) {
            //echo ($this->input->post('titulo'));
            //echo ($this->input->post('message'));
            echo ("Error Validar");
            // $this->index();
        } else {
            // Por el contrario se efectuará el proceso de registro.
            $titulo = $this->input->post('titulo');
            $cuerpo = $this->input->post('message');
            $usuario_id = $this->input->post('usuario_id');
            $categoria_id = $this->input->post('categoria_id');

            // Inserta los datos pasados.
            $this->forum_model->guardarDatosTemaNuevo($titulo, $cuerpo, $usuario_id, $categoria_id);

            echo TRUE;
        }
    }

    // Registra un tema nuevo con un archivo incluido.
    public function tema_nuevo_archivo() {        
        // Valida los  datos.
        $this->form_validation->set_rules('titulo', 'titulotema', 'required|trim|xss_clean|htmlentities');
        $this->form_validation->set_rules('message', 'mentema', 'required|trim|xss_clean|htmlentities');

        // Si la validación es inválida, devolverá error. 
        if ($this->form_validation->run() == FALSE) {
            echo ("Error Validar");
            // $this->index();
        } else {
            //Verifica si el tipo de archivo está permitido.
            //y que el tamaño del archivo no exceda los 1000kb = 1MB (mas o menos).
            $extensiones_permitidas = array("image/jpg", "image/jpeg", "image/gif", "image/png", "application/pdf");
            $limite_kb = 20480;
            if (in_array($_FILES['archivo']['type'], $extensiones_permitidas) && $_FILES['archivo']['size'] <= $limite_kb * 1024) {
                //Obtiene el archivo a subir.
                $archivo = $_FILES['archivo']['name'];
                // Guarda el archivo en otra variable antes d ecambiarle el nombre.
                $archivoOriginal = $archivo;
                // Separa la extensión del archivo del nombre.
                $tmp = explode('.', $_FILES['archivo']['name']);
                $tipoArchivo = end($tmp); //este es el tipo de archivo que acabas de subir       
                // Recupera los datos pasados por el formulario.
                $usuario_id = $this->input->post('id_usuario');
                $categoria_id = $this->input->post('id_categoria');
                $titulo = $this->input->post('titulo');
                $cuerpo = $this->input->post('message');
                // Crea la fecha de hoy sin espacios. Ej. 2016124120725 (AñoMesDiaHoraMinSegundos)
                $hoy = date('YmdHis');
                // Configura el nombre que se le va a dar al nuevo archivo.
                $nuevo_nombre = $categoria_id . $usuario_id . $hoy;
                // Renombra el archivo subido.
                $archivo = $nuevo_nombre . "." . $tipoArchivo;

                //Comprueba si existe el directorio para subir el archivo.
                //Si no es así, se crea y se le dan los permisos.
                if (!is_dir("files/")) {
                    mkdir("files/", 0777);
                }
                // Guarda el archivo renombrado al servidor
                move_uploaded_file($_FILES['archivo']['tmp_name'], "files/" . $archivo);

                // Inserta los datos pasados.
                $this->forum_model->guardarDatosTemaNuevo($titulo, $cuerpo, $usuario_id, $categoria_id, $archivoOriginal, $archivo);

                // Recarga la página.
               //header('Location: 40/recursos');
               echo TRUE;
            }else{
                echo "No permitido: Solo se permten archivos con extensión .pdf .jpg .jpeg .png o el archivo es demasiado grande";
            }
        }
    }

    // Registra un tema nuevo.
    public function responderTema() {

        $this->form_validation->set_rules('message', 'mentema', 'required|trim|xss_clean|htmlentities');

        // Si la validación es inválida, devolverá error. 
        if ($this->form_validation->run() == FALSE) {
            echo ("Error Validar");
            // $this->index();
        } else {
            // Por el contrario se efectuará el proceso de registro.            
            $cuerpo = $this->input->post('message');
            $usuario_id = $this->input->post('usuario_id');
            $tema_id = $this->input->post('tema_id');

            // Inserta los datos pasados.
            $this->forum_model->nueva_respuesta($cuerpo, $usuario_id, $tema_id);

            echo TRUE;
        }
    }

}
