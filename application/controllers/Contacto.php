<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'form_validation'));
        $this->load->helper(array('url', 'form', 'security'));
    }

    public function index() {
        //  Hace un switch para saber si existe la sesión, y en ese caso saber que contiene para poder redirigir a cada usuario a la página correspondiente.
        switch ($this->session->userdata('rol')) {
            case '':
                $data['token'] = $this->token();
                $data['titulo'] = 'Contacte';
                $this->load->view('header', $data);
                $this->load->view('contacto');
                $this->load->view('footer');
                break;
            case 'Administrador':
                //redirect(base_url() . 'admin');
                $data['titulo'] = 'Contacte';
                $this->load->view('headerSession', $data);
                $this->load->view('contacto');
                $this->load->view('footer');
                break;
            case 'Moderador':
                //redirect(base_url() . 'jefe');
                break;
            case 'Comun':
                $data['titulo'] = 'Contacte';
                $this->load->view('headerSession', $data);
                $this->load->view('contacto');
                $this->load->view('footer');
                //redirect(base_url() . 'empleado');
                break;
            default:
                $data['token'] = $this->token();
                $data['titulo'] = 'Contacte';
                $this->load->view('header');
                $this->load->view('contacto', $data);
                $this->load->view('footer');
                break;
        }
    }

    public function enviarEmailContacto() {

        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('asunto', 'Asunto', 'trim|required|xss_clean');
        $this->form_validation->set_rules('mensaje', 'Mensaje', 'required|xss_clean');

        if ($this->form_validation->run()) {
            $this->load->library('email');

            $destinatario = "comunitatdocent@gmail.com";
            $nombre = $this->input->post('nombre');
            $email = $this->input->post('email');
            $asunto = $this->input->post('asunto');
            $mensaje = $this->input->post('mensaje');

//configuracion para gmail
            $configGmail = array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.gmail.com',
                'smtp_port' => 465,
                'smtp_user' => 'comunitatdocent@gmail.com',
                'smtp_pass' => 'ComunitatDocent2016',
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'newline' => "\r\n"
            );

            //cargamos la configuración para enviar con gmail
            $this->email->initialize($configGmail);

            $this->email->from($email, $nombre);
            $this->email->to($destinatario);            
            $this->email->subject($asunto);
            $this->email->message('<html lang="ca">
<head>
    <meta charset="UTF-8"/>
    <title>Email Contacte Comunitat Docent</title>
</head>
<body>
   		<div class="contenido" style="text-align: center">
           <table class="tablaEmail" style="width: 100%;text-align: left;background: #e3f2fd">
               <tr>
                    <td><img src="http://comunitatdocent.cat/prova/assets/img/logotipo/ComIcon.png" alt="" style="max-width: 75px;max-height: 75px"/></td>
                    <td><h4 class="empresa" style="margin-left: 40em;float: right">Comunitat Docent</h4></td>
               </tr>
           </table>
           <table class="tablaEmail2" style="width: 100%;text-align: left;background: #e3f2fd">
                <tr>
                   <td><b>De '.$nombre.' &lt;'.$email.'&gt;</b> a Comunitat Docent</td>                   
               </tr>               
               <tr>
                   <td>'.$mensaje.'
                   </td>                   
               </tr>
           </table>
       </div>
    
</body>
</html>');
            $this->email->send();
            //con esto podemos ver el resultado
            //var_dump($this->email->print_debugger());

            redirect(base_url("contacto"));
            
        } else {
            echo "Error al validar formulario";
            //index();
        }
    }

    // Crea una nueva clave aleatoria que será la que contendrá nuestro formulario, de esta forma se evita el Cross-Site Request Forgery. 
    public function token() {
        $token = md5(uniqid(rand(), true));
        $this->session->set_userdata('token', $token);
        return $token;
    }

}
