<?php

class Forum_model extends CI_Model {

    public $title;
    public $content;
    public $date;

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    // Método que lista categorias del foro.
    function listarCategorias($padre = 0) {
        $this->db->where("padre", $padre);
        $q = $this->db->get('forum_categoria');
        return $q;
    }

    // Método que recupera el nombre de la categoria pasada por id.
    function nombreCategoria($id = 0) {
        $this->db->where("id", $id);
        $this->db->select("nombre");
        $q = $this->db->get("forum_categoria");
        $r = $q->row();
        return $r->nombre;
    }

    // Método que lista los temas según el id de la categoria.
    function listarTemas($categoria_id = 0) {
        $this->db->select("id,titulo, DATE_FORMAT(fecha, '%d/%m/%Y %T') AS fecha, nombre, fecha AS fecha_sql");
        $this->db->join('usuarios', 'usuario_id = idUsuario');
        $this->db->where("categoria_id", $categoria_id);
        $this->db->order_by("fecha_sql", "DESC");
        //$this->db->limit($limit);
        $q = $this->db->get("forum_temas");
        return $q;
    }

    // Método que recogerá la última respuesta a un tema.
    function ultima_respuesta($categoria_id, $tema_id) {
        $this->db->select("u.nombre AS nombreUsuario,DATE_FORMAT(fp.fecha, '%d/%m/%Y %T') AS fechaEU");
        $this->db->join('forum_posts fp', 'ft.id = fp.tema_id');
        $this->db->join('usuarios u', 'fp.usuario_id = u.idUsuario');
        $this->db->where('ft.categoria_id', $categoria_id);
        $this->db->where('tema_id', $tema_id);
        $this->db->order_by("fp.fecha", "DESC");
        $this->db->limit(1);

        $q = $this->db->get("forum_temas ft");
        return $q;
    }

    // Método que contará el número de respuestas que tiene un tema.
    function numero_respuestas($categoria_id, $tema_id) {
        $this->db->select("COUNT(fp.fecha)-1 AS numero_respuestas");
        $this->db->join('forum_posts fp', 'ft.id = fp.tema_id');
        $this->db->where('ft.categoria_id', $categoria_id);
        $this->db->where('tema_id', $tema_id);

        $q = $this->db->get("forum_temas ft");
        return $q;
    }

    //Obtiene el total de filas de temas de una categoria.
    public function num_rows($categoria_id = 0) {
        $this->db->where("categoria_id", $categoria_id);
        $consulta = $this->db->get('forum_temas');
        return $consulta->num_rows();
    }

    // Método que lista los posts segun el id del tema.
    function listarPosts($tema_id = 0) {
        $this->db->select("id,cuerpo, DATE_FORMAT(fecha, '%d/%m/%Y %T') AS fecha_normal, fecha, nombre, usuario_id, nombre_rango, archivo, archivoOriginal");
        $this->db->join('usuarios', 'usuario_id = idUsuario');
        $this->db->join('rango', 'rango = id_rango');
        $this->db->where("tema_id", $tema_id);
        $this->db->order_by("fecha", "ASC");
        $q = $this->db->get("forum_posts");
        return $q;
    }

    // Método que recoge el número de mensajes que ha escrito un usuario en particular.
    function numeroMensajes($id_usuario) {
        //select count(id) as numero_mensajes from forum_posts join usuarios on usuario_id = idUsuario where usuario_id = 1
        $this->db->select("COUNT(id) AS numero_mensajes");
        $this->db->join('usuarios', 'usuario_id = idUsuario');
        $this->db->where("usuario_id", $id_usuario);
        $q = $this->db->get("forum_posts");
        $r = $q->row();
        return $r->numero_mensajes;
    }

    function listarPostsTemaParam($id, $param) {
        $this->db->where("id", $id);
        $this->db->select($param);
        $q = $this->db->get("forum_temas");
        $r = $q->row_array();
        return $r[$param];
    }

    // Método que muestra el último tema creado según el id de la categoria.
    function ultimoTema($id_categoria) {
        $this->db->where("categoria_id", $id_categoria);
        $this->db->order_by("fecha", "desc");
        $this->db->limit(1);
        $this->db->select("id, titulo");
        $q = $this->db->get("forum_temas");
        return $q;
    }

    // Método que muestra el número de temas creados en cierta categoria según el id de la categoria.
    function numeroTemas($id_categoria) {
        $this->db->where("categoria_id", $id_categoria);
        $this->db->select("COUNT(titulo) AS 'numeroTemas'", FALSE);
        $q = $this->db->get("forum_temas");
        return $q->row();
    }

    // Método que inserta un nuevo tema con su primer mensaje.
    public function guardarDatosTemaNuevo($titulo, $cuerpo, $usuario_id, $categoria_id, $archivoOriginal = NULL, $archivo = NULL) {
        $ahora = date('Y-m-d H:i:s');
        
        $dataTema = array(
            'titulo' => $titulo,
            'fecha' => $ahora,
            'usuario_id' => $usuario_id,
            'categoria_id' => $categoria_id,
        );

        $this->db->insert('forum_temas', $dataTema);

        $this->db->where("titulo", $titulo);
        $this->db->order_by("fecha", "DESC");
        $this->db->select("id", FALSE);
        $q = $this->db->get("forum_temas");

        $id_tema = $q->row();

        $dataPost = array(
            'cuerpo' => $cuerpo,
            'fecha' => $ahora,
            'principal' => 1,
            'usuario_id' => $usuario_id,
            'tema_id' => $id_tema->id,
            'archivoOriginal' => $archivoOriginal,
            'archivo' => $archivo
        );

        $this->db->insert('forum_posts', $dataPost);
    }

    // Método que inserta una respuesta a un tema en concreto.
    public function nueva_respuesta($cuerpo, $usuario_id, $tema_id) {
        $ahora = date('Y-m-d H:i:s');

        $dataPost = array(
            'cuerpo' => $cuerpo,
            'fecha' => $ahora,
            'principal' => 0,
            'usuario_id' => $usuario_id,
            'tema_id' => $tema_id
        );

        $this->db->insert('forum_posts', $dataPost);
    }

}
