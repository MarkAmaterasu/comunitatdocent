$(document).ready(function () {
    var contN = false;
    var contC = false;
    var contS = false;
    var contI = false;
    var contL = false;

    // Inserta los tags en el textarea.
    $(".negrita").click(function () {
        if (contN === false) {
            $('#message').val($('#message').val() + '[negreta]').focus();
            contN = true;
        } else {
            $('#message').val($('#message').val() + '[/negreta]').focus();
            contN = false;
        }
    });

    $(".cursiva").click(function () {
        if (contC === false) {
            $('#message').val($('#message').val() + '[cursiva]').focus();
            contC = true;
        } else {
            $('#message').val($('#message').val() + '[/cursiva]').focus();
            contC = false;
        }
    });

    $(".subrayado").click(function () {
        if (contS === false) {
            $('#message').val($('#message').val() + '[sub]').focus();
            contS = true;
        } else {
            $('#message').val($('#message').val() + '[/sub]').focus();
            contS = false;
        }
    });

    $(".imagen").click(function () {
        var imagen = prompt("Si us plau, escriu l'enllaç de l'imatge aquí", "http://");
        if (imagen !== null) {
            $('#message').val($('#message').val() + '[img urlimg=' + imagen + ']');
        }

    });

    $(".link").click(function () {
        var enlace = prompt("Si us plau, escriu l'enllaç aquí", "http://");
        if (enlace !== null) {
            $('#message').val($('#message').val() + '[enlace url=' + enlace + ']' + enlace + '[/enlace]');
        }
    });

    $(".lista").click(function () {
        if (contL === false) {
            $('#message').val($('#message').val() + '[llista]').focus();
            contL = true;
        } else {
            $('#message').val($('#message').val() + '[/llista]').focus();
            contL = false;
        }
    });

    $(".linea").click(function () {
            $('#message').val($('#message').val() + '[punt]').focus();          
    });

    function getCookie(c_name) {
        var c_value = document.cookie;
        var c_start = c_value.indexOf(" " + c_name + "=");
        if (c_start == -1) {
            c_start = c_value.indexOf(c_name + "=");
        }
        if (c_start == -1) {
            c_value = null;
        } else {
            c_start = c_value.indexOf("=", c_start) + 1;
            var c_end = c_value.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = c_value.length;
            }
            c_value = unescape(c_value.substring(c_start, c_end));
        }
        return c_value;
    }

    function setCookie(c_name, value, exdays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
        document.cookie = c_name + "=" + c_value;
    }

    if (getCookie('ComunitatDocent') != "1") {
        document.getElementById("barraaceptacion").style.display = "block";
    }

    $("#ponerCookie").click(function () {
        setCookie('ComunitatDocent', '1', 365);
        document.getElementById("barraaceptacion").style.display = "none";
    });

    // Inicializa el zoom a las imágenes insertadas.
    $('.materialboxed').materialbox();

});