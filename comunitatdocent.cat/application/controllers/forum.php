<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Forum extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'form_validation', 'cifrado'));
        $this->load->helper(array('form', 'security'));
        $this->load->model('forum_model');
    }

    public function index() {
        //  Hace un switch para saber si existe la sesión, y en ese caso saber que contiene para poder redirigir a cada usuario a la página correspondiente.
        switch ($this->session->userdata('rol')) {
            case '':
                $data['categorias'] = $this->forum_model->listarCategorias();
                $data['token'] = $this->token();
                $data['titulo'] = 'Fòrum';
                $this->load->view('header', $data);
                $this->load->view('forum');
                $this->load->view('footer');
                break;
            case 'Administrador':
                //redirect(base_url() . 'admin');
                $this->load->view('headerSession');
                $this->load->view('forum');
                $this->load->view('footer');
                break;
            case 'Moderador':
                //redirect(base_url() . 'jefe');
                break;
            case 'Comun':
                $this->load->view('headerSession');
                $this->load->view('forum');
                $this->load->view('footer');
                //redirect(base_url() . 'empleado');
                break;
            default:
                $data['token'] = $this->token();
                $this->load->view('header');
                $this->load->view('forum', $data);
                $this->load->view('footer');
                break;
        }
    }

    function categoria($id = 0) {
        if ((int) $id <= 0) {
            echo "Error, no existe una categoría con esta id.";
        } else {
            //  Hace un switch para saber si existe la sesión, y en ese caso saber que contiene para poder redirigir a cada usuario a la página correspondiente.
            switch ($this->session->userdata('rol')) {
                case '':
                    $data['temas'] = $this->forum_model->listarTemas($id);
                    $data['titulo'] = 'Fòrum';
                    $data['token'] = $this->token();
                    $this->load->view('header', $data);
                    $this->load->view('forum/categoria');
                    $this->load->view('footer');
                    break;
                case 'Administrador':
                    //redirect(base_url() . 'admin');
                    $this->load->view('headerSession');
                    $this->load->view('forum/categoria');
                    $this->load->view('footer');
                    break;
                case 'Moderador':
                    //redirect(base_url() . 'jefe');
                    break;
                case 'Comun':
                    $this->load->view('headerSession');
                    $this->load->view('forum/categoria');
                    $this->load->view('footer');
                    //redirect(base_url() . 'empleado');
                    break;
                default:
                    $data['token'] = $this->token();
                    $this->load->view('header');
                    $this->load->view('forum/categoria', $data);
                    $this->load->view('footer');
                    break;
            }
        }
    }

    function post($id = 0) {
        if ((int) $id <= 0) {
            echo "Error, no existe un tema con esta id.";
        } else {

            //  Hace un switch para saber si existe la sesión, y en ese caso saber que contiene para poder redirigir a cada usuario a la página correspondiente.
            switch ($this->session->userdata('rol')) {
                case '':
                    $data['posts'] = $this->forum_model->listarPosts($id);
                    $data['titulo'] = 'Fòrum';
                    $data['token'] = $this->token();
                    $this->load->view('header', $data);
                    $this->load->view("forum/tema");
                    $this->load->view('footer');
                    break;
                case 'Administrador':
                    //redirect(base_url() . 'admin');
                    $this->load->view('headerSession');
                    $this->load->view("forum/tema");
                    $this->load->view('footer');
                    break;
                case 'Moderador':
                    //redirect(base_url() . 'jefe');
                    break;
                case 'Comun':
                    $this->load->view('headerSession');
                    $this->load->view("forum/tema");
                    $this->load->view('footer');
                    //redirect(base_url() . 'empleado');
                    break;
                default:
                    $data['token'] = $this->token();
                    $this->load->view('header', $data);
                    $this->load->view("forum/tema");
                    $this->load->view('footer');
                    break;
            }
        }
    }
    
        function nuevo_tema($id = 0) {
        if ((int) $id <= 0) {
            echo "Error, no existe un tema con esta id.";
        } else {

            //  Hace un switch para saber si existe la sesión, y en ese caso saber que contiene para poder redirigir a cada usuario a la página correspondiente.
            switch ($this->session->userdata('rol')) {
                case '':
                   // $data['posts'] = $this->forum_model->listarPosts($id);
                    $data['titulo'] = 'Fòrum';
                    $data['token'] = $this->token();
                    $this->load->view('header', $data);
                    $this->load->view("forum/nuevo_tema");
                    $this->load->view('footer');
                    break;
                case 'Administrador':                    
                    $this->load->view('headerSession');
                    $this->load->view("forum/nuevo_tema");
                    $this->load->view('footer');
                    break;
                case 'Moderador':
                    //redirect(base_url() . 'jefe');
                    break;
                case 'Comun':
                    $this->load->view('headerSession');
                    $this->load->view("forum/nuevo_tema");
                    $this->load->view('footer');
                    //redirect(base_url() . 'empleado');
                    break;
                default:
                    $data['token'] = $this->token();
                    $this->load->view('header', $data);
                    $this->load->view("forum/nuevo_tema");
                    $this->load->view('footer');
                    break;
            }
        }
    }

    // Crea una nueva clave aleatoria que será la que contendrá nuestro formulario, de esta forma se evita el Cross-Site Request Forgery. 
    public function token() {
        $token = md5(uniqid(rand(), true));
        $this->session->set_userdata('token', $token);
        return $token;
    }

}
