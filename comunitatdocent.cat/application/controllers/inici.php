<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Inici extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    
    public function __construct() {
        parent::__construct();
        $this->load->model('inici_model');
        $this->load->helper(array('url','form','security'));
        $this->load->library(array('form_validation'));
    }

     public function index() {
        //  Hace un switch para saber si existe la sesión, y en ese caso saber que contiene para poder redirigir a cada usuario a la página correspondiente.
        switch ($this->session->userdata('rol')) {
            case '':
                $data['token'] = $this->token();
                $data['titulo'] = 'Inici';
  
                $this->load->view('header', $data);
                
                $this->load->view('inici');
                $this->load->view('footer');
                break;
            case 'Administrador':
                
                $this->load->view('headerSession');
                $this->load->view('inici');
                $this->load->view('footer');
                break;
            case 'Moderador':
                
                break;
            case 'Comun':
                $this->load->view('headerSession');
                $this->load->view('inici');
                $this->load->view('footer');
                
                break;
            default:

                $this->load->view('header');
                
                $this->load->view('inici', $data);
                $this->load->view('footer');
                break;
        }
    }
    
        // Crea una nueva clave aleatoria que será la que contendrá nuestro formulario, de esta forma se evita el Cross-Site Request Forgery. 
    public function token() {
        $token = md5(uniqid(rand(), true));
        $this->session->set_userdata('token', $token);
        return $token;
    }
}
