<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('usuario_model');
        $this->load->helper(array('url','form','security'));
        $this->load->library(array('session','form_validation','cifrado'));
    }

    // Valida los datos introducidos en el formulario, envia los datos al modelo para procesarlos y efectuar el login del usuario.
    public function login() {

        if ($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {

            $this->form_validation->set_rules('emailSesion', 'Email', 'required|trim|valid_email|xss_clean');
            $this->form_validation->set_rules('passwordSesion', 'Password', 'required|trim|xss_clean');
           
            // Si la validación es inválida, se retornará a la página de inicio. 
            if ($this->form_validation->run() == FALSE) {
                echo ("Error Validar");
               // $this->index();
            } else {
                // Por el contrario se efectuará el proceso de loguin.
                $email = $this->input->post('emailSesion');
                //$password = $this->input->post('inputPassword');
                $password = $this->cifrado->crear(ALGORITMO,$this->input->post('passwordSesion'),LLAVE_CIFRADO);
                $check_user = $this->usuario_model->login_usuario($email, $password);

                if ($check_user == TRUE) {
                    $data = array(
                        'is_logued_in' => TRUE,
                        'id_usuario' => $check_user->idUsuario,
                        'rol' => $check_user->nombre_rol,
                        'nombre' => $check_user->nombre,
                        'apellido' => $check_user->apellido                        
                    );
                    $this->session->set_userdata($data);
                    echo TRUE;
                }else{
                    // El nombre de usuario o la contraseña son erroneas.
                    echo ("Error Datos.");
                }
            }
        } else {
            
            echo ("Error Token");
        }
    }

    // Registra un usuario nuevo.
    public function registro(){

         if ($this->input->post('tokenRegistro') && $this->input->post('tokenRegistro') == $this->session->userdata('token')) {

            $this->form_validation->set_rules('emailRegistro', 'Email', 'required|trim|valid_email|xss_clean');
            $this->form_validation->set_rules('passwordRegistro', 'Password', 'required|trim|xss_clean');
            $this->form_validation->set_rules('nombreRegistro', 'required|trim|xss_clean');
           
            // Si la validación es inválida, devolverá error. 
            if ($this->form_validation->run() == FALSE) {
                echo ("Error Validar");
               // $this->index();
            } else {
                // Por el contrario se efectuará el proceso de registro.
                $email = $this->input->post('emailRegistro');
                $nombre = $this->input->post('nombreRegistro');
                //$password = $this->input->post('inputPassword');
                $password = $this->cifrado->crear(ALGORITMO,$this->input->post('passwordRegistro'),LLAVE_CIFRADO);

                // Inserta los datos pasados.
                $check_registro = $this->usuario_model->guardarDatosRegistro($nombre,$email, $password);

                echo TRUE;

            }
        } else {
            
            echo ("Error Token");
        }

    }
     
    // Crea una nueva clave aleatoria que será la que contendrá nuestro formulario, de esta forma se evita el Cross-Site Request Forgery. 
    public function token() {
        $token = md5(uniqid(rand(), true));
        $this->session->set_userdata('token', $token);
        return $token;
    }
    

    // Elimina la sesión.
    public function logout() {
        // Destruye la sesión.
        $this->session->sess_destroy();
        // Redirige a la vista principal de inicio.
        redirect(base_url());
    }

}

