<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        //  Hace un switch para saber si existe la sesión, y en ese caso saber que contiene para poder redirigir a cada usuario a la página correspondiente.
        switch ($this->session->userdata('rol')) {
            case '':
                $data['token'] = $this->token();  
                $data['titulo'] = 'Contacte';
                $this->load->view('header', $data);                
                $this->load->view('contacto');
                $this->load->view('footer');
                break;
            case 'Administrador':
                //redirect(base_url() . 'admin');
                $this->load->view('headerSession');
                $this->load->view('contacto');
                $this->load->view('footer');
                break;
            case 'Moderador':
                //redirect(base_url() . 'jefe');
                break;
            case 'Comun':
                $this->load->view('headerSession');
                $this->load->view('contacto');
                $this->load->view('footer');
                //redirect(base_url() . 'empleado');
                break;
            default:
                $data['token'] = $this->token();  
                $this->load->view('header');                
                $this->load->view('contacto', $data);
                $this->load->view('footer');
                break;
        }
    }

            // Crea una nueva clave aleatoria que será la que contendrá nuestro formulario, de esta forma se evita el Cross-Site Request Forgery. 
    public function token() {
        $token = md5(uniqid(rand(), true));
        $this->session->set_userdata('token', $token);
        return $token;
    }

}