<?php

class Usuario_model extends CI_Model {

    public $title;
    public $content;
    public $date;

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    // Busca que sean coincidentes los datos insertados con los existentes en la base de datos.
    public function login_usuario($email, $password) {
        
       $this->db->select('idUsuario, u.nombre, u.apellido, r.nombre AS nombre_rol FROM usuarios AS u JOIN rol AS r ON u.id_rol = r.id_rol WHERE contrasenya = "'.$password.'" AND correo_electronico = "'.$email.'"', FALSE);        
        //$this->db->where('correo_electronico', $email);
        //$this->db->where('contrasenya', $password);
        $query = $this->db->get();
        
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    // Inserta los datos pasados de un usuario nuevo.
    public function guardarDatosRegistro($nombre, $email, $contrasenya) {

        $data = array(
            'nombre' => $nombre,
            'correo_electronico' => $email,
            'contrasenya' => $contrasenya,
        );

        $this->db->insert('usuarios', $data);

    }

}

?>