<?php

class Forum_model extends CI_Model {

    public $title;
    public $content;
    public $date;

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    // Método que lista categorias del foro.
    function listarCategorias($padre = 0) {
        $this->db->where("padre", $padre);
        $q = $this->db->get('forum_categoria');
        return $q;
    }

    // Método que recupera el nombre de la categoria pasada por id.
    function nombreCategoria($id = 0) {
        $this->db->where("id", $id);
        $this->db->select("nombre");
        $q = $this->db->get("forum_categoria");
        $r = $q->row();
        return $r->nombre;
    }

    // Método que lista los temas según el id de la categoria.
    function listarTemas($categoria_id = 0) {
        $this->db->select("id,titulo, DATE_FORMAT(fecha, '%d/%m/%Y %T') AS fecha, nombre");
        $this->db->join('usuarios', 'usuario_id = idUsuario');
        $this->db->where("categoria_id", $categoria_id);
        $this->db->order_by("fecha", "DESC");
        $q = $this->db->get("forum_temas");
        return $q;
    }

    // Método que lista los posts segun el id del tema.
    function listarPosts($tema_id = 0) {
        //select id, cuerpo, fecha, nombre from forum_posts join usuarios on usuario_id = idUsuario where tema_id = 3 order by fecha  asc
        $this->db->select("id,cuerpo, DATE_FORMAT(fecha, '%d/%m/%Y %T') AS fecha, nombre, usuario_id, nombre_rango");
        $this->db->join('usuarios', 'usuario_id = idUsuario');
        $this->db->join('rango', 'rango = id_rango');
        $this->db->where("tema_id", $tema_id);
        $this->db->order_by("fecha", "ASC");
        $q = $this->db->get("forum_posts");
        return $q;
    }
       
    // Método que recoge el número de mensajes que ha escrito un usuario en particular.
    function numeroMensajes($id_usuario) {
        //select count(id) as numero_mensajes from forum_posts join usuarios on usuario_id = idUsuario where usuario_id = 1
        $this->db->select("COUNT(id) AS numero_mensajes");
        $this->db->join('usuarios', 'usuario_id = idUsuario');
        $this->db->where("usuario_id", $id_usuario);
        $q = $this->db->get("forum_posts");
        $r = $q->row();
        return $r->numero_mensajes;
    }

    function listarPostsTemaParam($id, $param) {
        $this->db->where("id", $id);
        $this->db->select($param);
        $q = $this->db->get("forum_temas");
        $r = $q->row_array();
        return $r[$param];
    }

    // Método que muestra el último tema creado según el id de la categoria.
    function ultimoTema($id_categoria) {
        $this->db->where("categoria_id", $id_categoria);
        $this->db->order_by("fecha", "desc");
        $this->db->limit(1);
        $this->db->select("id, titulo");
        $q = $this->db->get("forum_temas");
        return $q;
    }

    // Método que muestra el número de temas creados en cierta categoria según el id de la categoria.
    function numeroTemas($id_categoria) {
        //SELECT COUNT(titulo) as 'numero temas' FROM forum_temas WHERE categoria_id = 3;
        $this->db->where("categoria_id", $id_categoria);
        $this->db->select("COUNT(titulo) AS 'numeroTemas'", FALSE);
        $q = $this->db->get("forum_temas");
        return $q->row();
    }

}
