</main>
<footer class="page-footer blue lighten-2">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Footer Content</h5>
            </div>
            <div class="col l4 offset-l2 s12">
                <div class="col s6">
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="#!">Inici</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Noticies</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Consells</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Recursos</a></li>
                    </ul>
                </div>
                <div class="col s6">
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="#!">Infantil</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Primària</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">ESO</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Batxillerat</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Cicles de Formació</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright blue">
        <div class="container">
            © 2015 Comunitat Docent
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
        </div>
    </div>
</footer>

<script>
    // Para que funcione el menú izquierdo en versión móvil.
    $(document).ready(function () {
        $(".noCuenta").click(function () {
            $(".cerrarModalInicio").trigger("click");            
        });
        $(".siCuenta").click(function () {
            $("#cerrarModalRegistro").trigger("click");         
        });

        $(".button-collapse").sideNav({
            menuWidth: 240, // Default is 240
            closeOnClick: true
        });
        // Para que funcione los modal.
        // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
        $('.modal-trigger').leanModal();
        // Para que funcione el dropdown del menú del usuasrio.
        $('.dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrain_width: true, // Does not change width of dropdown to that of the activator
            hover: true, // Activate on hover
            gutter: 0, // Spacing from edge
            belowOrigin: true, // Displays dropdown below the button
            alignment: 'left' // Displays dropdown with edge aligned to the left of button
        });

        // Valida y envia el formilario de registro.
        $("#formRegistro").validate({
            rules: {
                nombreRegistro: {
                    required: true,
                    minlength: 2
                },
                emailRegistro: {
                    required: true,
                    email: true
                },
                passwordRegistro: {
                    required: true,
                    minlength: 6
                },
                passwordRegistro2: {
                    required: true,
                    minlength: 6,
                    equalTo: "#passwordRegistro"
                },
                terminos: {required: true}
            },
            messages: {
                nombreRegistro: {
                    required: "Si us plau, escriu el teu nom.",
                    minlength: "El nom ha de tindre com a mínim dos caràcters."
                },
                emailRegistro: {
                    required: "Si us plau, escriu una direcció de correu vàlida.",
                    email: "Email no vàlid."
                },
                passwordRegistro: {
                    required: "Si us plau, escriu una contrasenya.",
                    minlength: "La seva contrasenya és massa curta, mínim 6 caràcters."
                },
                passwordRegistro2: {
                    required: "Si us plau, escriu una contrasenya.",
                    minlength: "La seva contrasenya és massa curta, mínim 6 caràcters.",
                    equalTo: "La contrasenya no coincideix amb l'anterior, ha de ser igual."
                },
                terminos: {
                    required: "Accepta els terminis si us plau."
                },
            },
            errorPlacement: function (error, element) {
                error.appendTo(element.parent("div").next("span"));
            }, submitHandler: function (form) {

                var nombre = $('form[name=formRegistro] input[name=nombreRegistro]')[0].value;
                var email = $('form[name=formRegistro] input[name=emailRegistro]')[0].value;
                var contrasenya = $('form[name=formRegistro] input[name=passwordRegistro]')[0].value;
                var contrasenya2 = $('form[name=formRegistro] input[name=passwordRegistro2]')[0].value;
                var token = $('form[name=formRegistro] input[name=tokenRegistro]')[0].value;

                $.ajax({
                    type: "POST",
                    url: "<?= BASE_URL; ?>usuario/registro",
                    data: {
                        nombreRegistro: nombre,
                        emailRegistro: email,
                        passwordRegistro: contrasenya,
                        passwordRegistro2: contrasenya2,
                        tokenRegistro: token
                    }
                })

                        .done(function (response) {
                            if (response == true) {
                                // .trigger en Jquery se encarga de hacer el evento señalado automáticamente.
                                // En este caso, hace click en los botones, enlaces, pasados por id.
                                $("#cerrarModalRegistro").trigger("click");
                                $("#conectar").trigger("click");

                                Materialize.toast('Registre efectuat amb éxit.', 10000);
                            } else {
                                var pattEmail = /email/g;
                                // Comprueba si existe o no "email" en el mensaje de error.
                                if (pattEmail.test(response)) {
                                    $("#emailRegistro").addClass('invalid');
                                    $("#emailRegistro").removeClass('valid');
                                    $("#emailRegistro").focus();
                                    $("#errorEmail").html("Aquest email ja ha estat registrat.");
                                }
                                ;
                            }
                        });
                return false;
            }
        });


        // Valida y envia el formilario de login.
        $("#formInicio").validate({
            rules: {
                emailSesion: {
                    required: true,
                    email: true
                },
                passwordSesion: {
                    required: true,
                    minlength: 6
                }
            },
            messages: {
                emailSesion: {
                    required: "Si us plau, escriu una direcció de correu vàlida.",
                    email: "Email no vàlid."
                },
                passwordSesion: {
                    required: "Si us plau, escriu una contrasenya.",
                    minlength: "La seva contrasenya és massa curta, mínim 6 caràcters."
                }
            },
            errorPlacement: function (error, element) {
                error.appendTo(element.parent("div").next("span"));
            }, submitHandler: function (form) {

                var contrasenya = $('form[name=formInicio] input[name=passwordSesion]')[0].value;
                var email = $('form[name=formInicio] input[name=emailSesion]')[0].value;
                var token = $('form[name=formInicio] input[name=token]')[0].value;

                $.ajax({
                    type: "POST",
                    url: "<?= BASE_URL; ?>usuario/login",
                    data: {
                        passwordSesion: contrasenya,
                        emailSesion: email,
                        token: token
                    }
                })

                        .done(function (response) {
                            if (response == true) {
                                location.reload();
                            } else {
                                var pattPass = /Pass/g;
                                var pattEmailSesion = /Email/g;
                                var pattDatos = /Datos/g;

                                if (pattPass.test(response)) {
                                    $("#passwordSesion").addClass('invalid');
                                    $("#passwordSesion").removeClass('valid');
                                    $("#passwordSesion").focus();
                                    $("#errorPass").html("Contrasenya equivocada.");
                                }
                                ;
                                if (pattEmailSesion.test(response)) {
                                    $("#emailSesion").addClass('invalid');
                                    $("#emailSesion").removeClass('valid');
                                    $("#emailSesion").focus();
                                    $("#errorEmailSesion").html("Aquest email no està registrat.");
                                }
                                ;

                                if (pattDatos.test(response)) {
                                    $("#emailSesion").focus();
                                    $("#errorEmailSesion").html("Contrasenya o email erronis.");
                                }
                                ;

                            }
                        });
                return false;
            }
        });
    });

</script>
</body>
</html>
