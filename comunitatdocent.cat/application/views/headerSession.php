<!DOCTYPE html>
<html lang="ca">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="description" content="Pàgina pensada per ayudar als professors y que colaborin entre ells.">
        <meta name="author" content="Marc Trallero García">
        <meta name="keywords" content="HTML5,CSS3,JQuery,MaterializeCSS,PHP">

        <title>Index</title>

        <!-- icono web-->
        <!--<link rel="shortcut icon" href="">-->

        <!--    Se cargan los estilos de materialize -->
        <link rel="stylesheet" href="<?= BASE_URL; ?>assets/css/materialize/css/materialize.min.css">
        <!-- Se cargan los iconos  -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">   
        <!-- Se cargan los estilos --> 
        <link rel="stylesheet" href="<?= BASE_URL; ?>assets/css/estilos.css">
        <!-- Se cargan el Jquery -->
        <script src="<?= BASE_URL; ?>assets/js/jquery-2.1.4.min.js"></script>
        <!-- Se cargan el JS de Materialize   -->
        <script src="<?= BASE_URL; ?>assets/css/materialize/js/materialize.min.js"></script>
        <!-- Se carga la librería de Jquery Validation -->
        <script src="<?= BASE_URL; ?>assets/js/jqueryValidation/dist/jquery.validate.js" type="text/javascript"></script>
        <script src="<?= BASE_URL; ?>assets/js/jquery.form.js" type="text/javascript"></script>

    </head>
    <body>
        <header>
            <!-- Nav superior  -->
            <nav class="nav1">
                <div class="nav-wrapper">
                    <a href="<?php echo BASE_URL; ?>inici" class="brand-logo">Comunitat Docent</a>
                    <!--     Botón para sacar el menú lateral móbil     -->
                    <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                    <!--          Menú principal-->
                    <div class="brand-logo center hide-on-med-and-down">
                        <form>
                            <div class="input-field">
                                <input id="buscar" type="search" placeholder="Cercar" required>
                                <label for="buscar"><i class="material-icons">search</i></label>
                                <i class="mdi-navigation-close close"></i>
                            </div>
                        </form>
                    </div>
                    <ul class="right hide-on-med-and-down">
                        <li class="col s12">
                            <img src="<?php echo BASE_URL; ?>assets/img/pers100.png" alt="" class="circle responsive-img imgSesion"> 
                        </li>
                        <li>
                            <a class='dropdown-button' href='#' data-activates='menuUsuario'>
                                <i class="tiny material-icons right black-text">keyboard_arrow_down</i>
                                <span class="black-text"><b><?= $this->session->userdata('nombre') . " " . $this->session->userdata('apellido') ?></b></span>                            
                            </a>
                            <ul id='menuUsuario' class='dropdown-content blue darken-1 valign-wrapper'>
                                <li><a class="black-text valign" href="#!"><i class="material-icons left">face</i>Perfil</a></li>
                                <li><a class="black-text" href="#!">Publicar</a></li>
                                <li class="divider"></li>
                                <li><?= anchor(base_url() . 'usuario/logout', '<i class="material-icons left sesionOff">power_settings_new</i><span class="sesionOff"><b>Desconnectar</b></span>') ?></li>
                            </ul> 
                        </li>
                        <li>
                            <a href="<?php echo BASE_URL; ?>contacto"><i class="tiny material-icons left">info</i>Contacte</a>
                        </li>
                        <li>
                            <a href="">Qui Som</a>
                        </li>
                    </ul>
                    <!--          -->
                    <!--    Menú lateral móbil      -->
                    <!--         -->
                    <ul class="side-nav center" id="mobile-demo">
                        <li>
                            <form>
                                <div class="input-field">
                                    <input id="buscarMobil" type="search" placeholder="Cercar" required>
                                    <label for="buscarMobil"><i class="material-icons">search</i></label>
                                </div>
                            </form>
                        </li>
                        <li class="divider"></li>
                        <li><img src="assets/img/User-info_bw.png" alt="" class="circle responsive-img imgSesionMob"></li>
                        <span><b><?= $this->session->userdata('nombre') . " " . $this->session->userdata('apellido') ?></b></span>
                        <li class="divider"></li>
                        <li><a href="">Perfil</a></li>
                        <li><a href="">Desconnectar</a></li>
                        <li class="divider"></li>
                        <i class="material-icons">info</i>
                        <li>
                            <a href="<?php echo BASE_URL; ?>contacto">Contacte</a>
                        </li>
                        <li>
                            <a href="">Qui Som</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo BASE_URL; ?>inici">Inici</a></li>
                        <li><a href="">Noticies</a></li>
                        <li><a href="">Consells</a></li>
                        <li><a href="">Recursos</a></li>
                        <li class="divider"></li>
                        <li><a href="">Infantil</a></li>
                        <li><a href="">Primària</a></li>
                        <li><a href="">ESO</a></li>
                        <li><a href="">Batxillerat</a></li>
                        <li><a href="">Cicles de Formació</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo BASE_URL; ?>forum">Fòrum</a></li>
                    </ul>
                </div>
            </nav>
            <!--      Nav inferior-->
            <nav class="hide-on-med-and-down z-depth-3 nav2">
                <div class="nav-wrapper container">
                    <ul class="left general">
                        <li><a href="<?php echo BASE_URL; ?>inici">Inici</a></li>
                        <li><a href="">Noticies</a></li>
                        <li><a href="">Consells</a></li>
                        <li><a href="">Recursos</a></li>
                    </ul>
                    <ul class="left etapas">
                        <li><a href="">Infantil</a></li>
                        <li><a href="">Primària</a></li>
                        <li><a href="">ESO</a></li>
                        <li><a href="">Batxillerat</a></li>
                        <li><a href="">Cicles de Formació</a></li>
                    </ul>
                    <ul class="left etapas">
                        <li><a href="<?php echo BASE_URL; ?>forum">Fòrum</a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <main>