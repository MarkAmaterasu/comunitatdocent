<div class="container contenido">
    <div class="row">
        <div class="col s12 m12 l12">

            <nav class="z-depth-2 breadcrumbForo col s10">
                <div class="nav-wrapper">
                    <div class="col s12">
                        <?php
                        echo anchor(BASE_URL . 'forum', 'Fòrum', array('class' => 'breadcrumb breadForum'));

                        echo '<span class="breadcrumb"><b class="breadCategoria">' . $this->forum_model->nombreCategoria((int) $this->uri->segment(2)) . '</b></span>';
                        ?>
                    </div>
                </div>
            </nav>
            <div class="col s2 valign-wrapper">
                <?php
                $categoriaNuevoTema = url_title(convert_accented_characters($this->forum_model->nombreCategoria((int) $this->uri->segment(2))), '-', TRUE);
                $urlNuevoTema = 'forum/' . $this->uri->segment(2) . '/' . $categoriaNuevoTema . '/';
                $urlNuevoTema .= url_title(convert_accented_characters("nou tema"), '-', TRUE);
                //echo anchor('#modal1', '<i class="material-icons left">note_add</i>Nou tema', array('class' => 'waves-effect waves-light btn modal-trigger btnNouTema valign  blue accent-4'));
                echo '<a class="waves-effect waves-light btn modal-trigger btnNouTema valign  blue accent-4" href="#modalTemaNuevo"><i class="material-icons left">note_add</i>Nou Tema</a>';
                ?>
            </div>

            <!-- Modal Nuevo Tema -->
            <div id="modalTemaNuevo" class="modal bottom-sheet">

                <div class="card-panel z-depth-1 col s12"> 

                    <div class="row col s3"><h4><b>Nou tema</b></h4></div>
                    <div class="row col s6">

                        <ul id="menuFormularioHtml">
                            <li class="instertObject"><i class="material-icons">add_a_photo</i></li>
                            <li class="instertObject ultiObj"><i class="material-icons">insert_link</i></li>
                            <li class="instertFormat"><i class="material-icons">format_bold</i></li>
                            <li class="instertFormat"><i class="material-icons">format_italic</i></li>
                            <li class="instertFormat ultiObj"><i class="material-icons">format_underlined</i></li>
                            <li class="instertList"><i class="material-icons">format_list_bulleted</i></li>
                            <li class="instertList"><i class="material-icons">linear_scale</i></li>
                        </ul> 

                        <div class="card-panel col s12 light-green lighten-5 cardTemaNuevo">
                            <form class="col s12">
                                <div class="row ">
                                    <div class="input-field">
                                        <input id="titulo" type="text">
                                        <label for="titulo">Títol</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field">
                                        <textarea id="mensaje" class="materialize-textarea"></textarea>
                                        <label for="mensaje">Missatge</label>
                                    </div>
                                </div>
                        </div>
                        <div class="row col s12"> 
                            <button class="btn waves-effect waves-light blue accent-4 right" type="submit" name="action">Publicar tema
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                        </form>

                    </div>
                    <div class="row col s3"></div>
                </div>
            </div>



            <?php
            echo '<div class="card-panel z-depth-1 col s12"> 
                <table class="striped tablaTemas">';
            echo '<thead>
                            <tr>
                                <th data-field="nomForum" class="col s4 m4 l4">Tema</th>
                                <th data-field="ultimTema" class="col s4 m4 l4 center-align">Autor</th>
                                <th data-field="nombreTemes" class="col s2 m2 l2 center-align">Últim missatge</th>
                                <th data-field="nombreTemes" class="col s1 m1 l1 center-align">Respostes</th>
                                <th data-field="nombreTemes" class="col s1 m1 l1 center-align">Lectures</th>
                            </tr>
                          </thead>
                          <tbody>';

            if ($temas->num_rows() > 0) {

                foreach ($temas->result() as $post) {
                    $categoria = url_title(convert_accented_characters($this->forum_model->nombreCategoria((int) $this->uri->segment(2))), '-', TRUE);
                    $url = 'forum/' . $categoria . '/' . $post->id . '/';
                    $url .= url_title(convert_accented_characters($post->titulo), '-', TRUE);
                    echo '<tr class="filaCategoriaForo"><td class="col s4 m4 l4 alturaT">' . anchor($url, $post->titulo) . '</td>';
                    echo '<td class="col s4 m4 l4 center-align">' . anchor($url, $post->nombre);
                    echo '<p class = "descripcionCategoria">' . $post->fecha . '</p></td>';
                    echo '</tr>';
                }
                echo '</tbody>
                          </table>
                    </div>';
            } else {
                echo '<p>No hi ha cap  tema creat.</p>';
            }
            ?>

        </div>
    </div>
</div>