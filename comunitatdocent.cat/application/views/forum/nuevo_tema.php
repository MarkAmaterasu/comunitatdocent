<div class="container contenido">
    <div class="row">
        <div class="col s12 m12 l12">

            <nav class="z-depth-2 breadcrumbForo col s12">
                <div class="nav-wrapper">
                    <div class="col s12">
                        <?php
                        echo anchor(BASE_URL . 'forum', 'Fòrum', array('class' => 'breadcrumb breadForum'));

                        echo anchor(BASE_URL . 'forum/' . $this->uri->segment(2) . '/' . $this->forum_model->nombreCategoria((int) $this->uri->segment(2)), $this->forum_model->nombreCategoria((int) $this->uri->segment(2)), array('class' => 'breadcrumb breadForum'));

                        echo '<span class="breadcrumb"><b class="breadCategoria"> Nou tema </b></span>';
                        ?>
                    </div>
                </div>
            </nav> 

            <div class="card-panel z-depth-1 col s12"> 

                <div class="row col s3"></div>
                <div class="row col s6">

                    <ul id="menuFormularioHtml">
                        <li class="instertObject"><i class="material-icons">add_a_photo</i></li>
                        <li class="instertObject ultiObj"><i class="material-icons">insert_link</i></li>
                        <li class="instertFormat"><i class="material-icons">format_bold</i></li>
                        <li class="instertFormat"><i class="material-icons">format_italic</i></li>
                        <li class="instertFormat ultiObj"><i class="material-icons">format_underlined</i></li>
                        <li class="instertList"><i class="material-icons">format_list_bulleted</i></li>
                        <li class="instertList"><i class="material-icons">linear_scale</i></li>
                    </ul> 

                    <div class="card-panel col s12 light-green lighten-5 cardTemaNuevo">
                        <form class="col s12">
                            <div class="row ">
                                <div class="input-field">
                                    <input id="titulo" type="text">
                                    <label for="titulo">Títol</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field">
                                    <textarea id="mensaje" class="materialize-textarea"></textarea>
                                    <label for="mensaje">Missatge</label>
                                </div>
                            </div>
                    </div>
                    <div class="row col s12"> 
                        <button class="btn waves-effect waves-light blue accent-4 right" type="submit" name="action">Nou Tema
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                    </form>

                </div>
                <div class="row col s3"></div>
            </div>



        </div>
    </div>
</div> 
