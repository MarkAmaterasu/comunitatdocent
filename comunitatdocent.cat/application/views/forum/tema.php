<div class="container contenido">
    <div class="row">
        <div class="col s12 m12 l12">
            
            <nav class="z-depth-2 breadcrumbForo breadPosts">
                <div class="nav-wrapper">
                    <div class="col s12 m12 l12">
                        <?php
                        $idCategoria = $this->forum_model->listarPostsTemaParam((int) $this->uri->segment(3), "categoria_id");
                        $categoria = url_title(convert_accented_characters($this->forum_model->NombreCategoria($this->forum_model->listarPostsTemaParam((int) $this->uri->segment(3), "categoria_id"))), '-', TRUE);

                        echo anchor(BASE_URL . 'forum', 'Fòrum', array('class' => 'breadcrumb breadForum'));
                        echo anchor(BASE_URL . 'forum/' . $idCategoria . '/' . $categoria, $this->forum_model->NombreCategoria($this->forum_model->listarPostsTemaParam((int) $this->uri->segment(3), "categoria_id")), array('class' => 'breadcrumb breadForum'));

                        echo '<span class="breadcrumb"><b class="breadCategoria">' . $this->forum_model->listarPostsTemaParam((int) $this->uri->segment(3), "titulo") . '</b></span>';
                        ?>
                    </div>
                </div>
            </nav> 


            <?php            
            if ($posts->num_rows() > 0) {
                foreach ($posts->result() as $post) {
                    $numero_mensajes = $this->forum_model->numeroMensajes($post->usuario_id);
                    echo '
                            <div class="card-panel z-depth-1 col s12 m12 l12 cardPost">
                                  <div class="card z-depth-1 col s2 m2 l2 cardUsuForo center-align">
                                    <div class="card-content activator">
                                        <img src="' . BASE_URL . 'assets/img/pers100.png" alt="" class="circle responsive-img activator" height="65" width="65"> 
                                                                    <div class="divider dividerForum"></div>
                                                                    <p card-title activator><b>' . $post->nombre . '</b></p>
                                                                    <p>';
                                                                    echo $post->nombre_rango;
                                                                    echo'</p>
                                    </div>
                                    <div class="card-reveal secre z-depth-5 left-align">
                                      <span class="card-title ">'.$post->nombre.'<i class="material-icons right">close</i></span>
                                      <p>Missatges:  '. $numero_mensajes.'</p>
                                    </div>
                                  </div>
                                <div class="col s10 m10 l10 center-align">';
                    echo '<p class="right-align fechaPost">' . $post->fecha . '</p>';
                    echo '<p>' . $post->cuerpo . '</p></div></div>';
                }
            } else {
                echo '<p>No hi ha cap tema creat.</p>';
            }
            ?>
        </div>
    </div>
</div>