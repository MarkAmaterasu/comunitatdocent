<?php

/**
 * @param String $algoritmo es el algoritmo usado para cifrar, por ejemplo 'MD5' o 'SHA1'.
 * @param String $datos será el String que querramos cifrar.
 * @param String $key será el hash creado por nosotros donde agregamos una cadena a la formación de la contraseña.
 *
 * Esta clase se encargará de cifrar datos, como las contraseñas.
 */
class Cifrado {

    public static function crear($algoritmo, $datos, $key) {

        $contexto = hash_init($algoritmo, HASH_HMAC, $key);
        hash_update($contexto, $datos);
        return hash_final($contexto);
    }

}

?>